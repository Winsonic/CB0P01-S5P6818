
重要事项：
	1、此源码包支持荣品电子科技三款开发板，分别是rp6818、king6818、pro6818

	2、默认支持的是pro6818，也就是说拿到不做修改的源码包，编译出来的是pro6818的镜像。
		kernel:kernel目录下输入命令:make ARCH=arm menuconfig
			Device Drivers  --->
				rongpin customized drivers  --->
					rpdzkj board select (RPDZKJ PRO6818)  --->	//选择的是pro6818的时候，此时的内核是pro6818的内核
		uboot:u-boot目录下文件u-boot/include/configs/s5p6818_drone.h
			//#define CONIFG_RP6818
			#define CONFIG_PRO6818	//CONFIG_PRO6818宏定义，此时的uboot是pro6818的uboot 
	
	3、如果开发板是rp6818或者是king6818，则需要做以下修改：
		kernel:kernel目录下输入命令:make ARCH=arm menuconfig
			Device Drivers  --->
				rongpin customized drivers  --->
					rpdzkj board select (RPDZKJ RP6818)  --->	//选择的是rp6818的时候，此时的内核是rp6818的内核
		uboot:u-boot目录下文件u-boot/include/configs/s5p6818_drone.h
			#define CONIFG_RP6818	//CONIFG_RP6818宏定义，此时的uboot是rp6818、king6818的uboot
			//#define CONFIG_PRO6818	//CONFIG_PRO6818宏定义，此时的uboot是rp6818、king6818的uboot 	





目录说明:
build/  编译工具及编译脚本
kernel/ QT内核源码，链接到linux\kernel\kernel-3.4.39
linux/  uboot 以及Kernel源码相关目录
result/ 编译完成后镜像生成的路径
u-boot/ QT uboot源码，链接到linux\bootloader\u-boot-2014.07

编译操作说明:
	在QT_6818目录下，
	编译uboot:
		. ./build/build_uboot.sh
	编译kernel:
		. ./build/build_kernel.sh
	打包system:
		. ./build/build_system.sh

烧录：
	和烧录android一样，用开发阶段用fastboot 烧录即可。
		
注意：
		1: 打包system 目录需要先su 切换到root 用户才能执行。
				