#
#
#
WINSONIC_OEM_PATH := OEM

#ADDITIONAL_DEFAULT_PROPERTIES += net.dns1=8.8.8.8

OEM_PATH ?= $(OEM_Name).$(Product_ID)

#PRODUCT_COPY_FILES += \
        $(call find-copy-subdir-files,*,$(WINSONIC_OEM_PATH)/generic/system,system)

PRODUCT_PROPERTY_OVERRIDES += 
	ro.oem.name=$(OEM_Name) \
	ro.oem.id=$(OEM_ID) \
	ro.oem.pid=$(Product_ID)

ifneq ($(strip $(OEM_Name)),)
-include $(WINSONIC_OEM_PATH)/$(OEM_PATH)/build.mk
else
# set default HDMI rotation lock
#PRODUCT_PROPERTY_OVERRIDES += \
    persist.demo.hdmirotationlock=1

# set default DPI
#PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=160
endif

