target_arch := $(strip $(shell cat result/arm_arch))
# $(warning target_arch: $(target_arch))

# secure
is_secure := $(shell cat kernel/.config | grep -q "CONFIG_SUPPORT_OPTEE_OS=y" && echo -n 1 || echo -n 0)

################################################################################
# kernel
################################################################################
kernel_patch_level := $(shell cat kernel/Makefile | grep "PATCHLEVEL =" | cut -f 3 -d ' ')
# $(warning kernel_patch_level: $(kernel_patch_level))
kernel_image :=
ifeq ($(strip $(target_arch)),32)
ifeq ($(strip $(kernel_patch_level)),4)
kernel_image := uImage
endif
ifeq ($(strip $(kernel_patch_level)),18)
kernel_image := zImage
endif
endif
ifeq ($(strip $(target_arch)),64)
kernel_image := Image
endif
# $(warning kernel_image: $(kernel_image))
PRODUCT_COPY_FILES += \
	kernel/arch/arm/boot/$(kernel_image):kernel

################################################################################
# bootloader
################################################################################
PRODUCT_COPY_FILES += \
	u-boot/u-boot.bin:bootloader

################################################################################
# 2ndboot
################################################################################
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/boot/2ndboot.bin:2ndbootloader

################################################################################
# init
################################################################################
ifeq ($(strip $(target_arch)),32)
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/init.s5p6818_drone.rc:root/init.s5p6818_drone.rc \
	device/nexell/s5p6818_drone/init.s5p6818_drone.usb.rc:root/init.s5p6818_drone.usb.rc \
	device/nexell/s5p6818_drone/ueventd.s5p6818_drone.rc:root/ueventd.s5p6818_drone.rc \
	device/nexell/s5p6818_drone/init.recovery.s5p6818_drone.rc:root/init.recovery.s5p6818_drone.rc
endif

ifeq ($(strip $(kernel_patch_level)),18)
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/fstab.s5p6818_drone64:root/fstab.s5p6818_drone
else
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/fstab.s5p6818_drone:root/fstab.s5p6818_drone
endif

ifeq ($(strip $(target_arch)),64)
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/init.s5p6818_drone64.rc:root/init.s5p6818_drone64.rc \
	device/nexell/s5p6818_drone/init.s5p6818_drone.usb.rc:root/init.s5p6818_drone64.usb.rc \
	device/nexell/s5p6818_drone/fstab.s5p6818_drone64:root/fstab.s5p6818_drone64 \
	device/nexell/s5p6818_drone/ueventd.s5p6818_drone.rc:root/ueventd.s5p6818_drone64.rc \
	device/nexell/s5p6818_drone/init.recovery.s5p6818_drone.rc:root/init.recovery.s5p6818_drone64.rc
endif

PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/adj_lowmem.sh:root/adj_lowmem.sh 
#	device/nexell/s5p6818_drone/bootanimation.zip:system/media/bootanimation.zip

PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/switch_hdmi_mode.sh:system/bin/switch_hdmi_mode.sh

################################################################################
# recovery 
################################################################################
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/busybox:busybox				\
	device/nexell/s5p6818_drone/busybox:system/xbin/busybox

################################################################################
# key
################################################################################
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/nxp_ir_recv.kl:system/usr/keylayout/nxp_ir_recv.kl \
	device/nexell/s5p6818_drone/keypad_s5p6818_drone.kl:system/usr/keylayout/keypad_s5p6818_drone.kl \
	device/nexell/s5p6818_drone/keypad_s5p6818_drone.kcm:system/usr/keychars/keypad_s5p6818_drone.kcm

################################################################################
# touch
################################################################################
#PRODUCT_COPY_FILES += \
#    device/nexell/s5p6818_drone/gslX680.idc:system/usr/idc/gslX680.idc

PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/ft5x06_ts.idc:system/usr/idc/ft5x06_ts.idc \
 
# eGalax USB/UART touch
PRODUCT_COPY_FILES += \
       device/nexell/s5p6818_drone/eGalaxTouch_VirtualDevice.idc:system/usr/idc/eGalaxTouch_VirtualDevice.idc \
       device/nexell/s5p6818_drone/echoEETIid.sh:system/bin/echoEETIid.sh \
       device/nexell/s5p6818_drone/eGTouchA.ini:system/etc/eGTouchA.ini \
       device/nexell/s5p6818_drone/eGTouchD:system/bin/eGTouchD

################################################################################
# camera
################################################################################
PRODUCT_PACKAGES += \
	camera.slsiap

ifeq ($(BOARD_HAS_CAMERA),true)
ifeq ($(USE_CAMERA_STUB),false)
ifeq ($(USE_PRBUILT_CAMERA_HAL),true)
#usb camera
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/usb_camera/lib/hw/camera.slsiap.so:system/lib/hw/camera.slsiap.so 
endif
endif
endif


################################################################################
# hwc executable
################################################################################
PRODUCT_PACKAGES += \
    report_hwc_scenario

################################################################################
# sensor
################################################################################
PRODUCT_PACKAGES += \
	sensors.s5p6818_drone

PRODUCT_COPY_FILES += \
    frameworks/av/media/libstagefright/data/media_codecs_google_audio.xml:system/etc/media_codecs_google_audio.xml \
    frameworks/av/media/libstagefright/data/media_codecs_google_video.xml:system/etc/media_codecs_google_video.xml

################################################################################
# audio
################################################################################
# mixer paths
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/audio/tiny_hw.s5p6818_drone.xml:system/etc/tiny_hw.s5p6818_drone.xml
# audio policy configuration
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/audio/audio_policy.conf:system/etc/audio_policy.conf

################################################################################
# media, camera
################################################################################
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/media_codecs.xml:system/etc/media_codecs.xml \
	device/nexell/s5p6818_drone/media_profiles.xml:system/etc/media_profiles.xml


################################################################################
# modules 
################################################################################
# ogl
ifeq ($(strip $(kernel_patch_level)),4)
PRODUCT_COPY_FILES += \
	hardware/samsung_slsi/slsiap/prebuilt/library/libVR.so:system/lib/libVR.so \
	hardware/samsung_slsi/slsiap/prebuilt/library/libEGL_vr.so:system/lib/egl/libEGL_vr.so \
	hardware/samsung_slsi/slsiap/prebuilt/library/libGLESv1_CM_vr.so:system/lib/egl/libGLESv1_CM_vr.so \
	hardware/samsung_slsi/slsiap/prebuilt/library/libGLESv2_vr.so:system/lib/egl/libGLESv2_vr.so

PRODUCT_COPY_FILES += \
	hardware/samsung_slsi/slsiap/prebuilt/modules/vr.ko:system/lib/modules/vr.ko
endif

ifeq ($(strip $(kernel_patch_level)),18)
PRODUCT_COPY_FILES += \
	hardware/samsung_slsi/slsiap/prebuilt/library/libGLES_mali.so.32:system/lib/egl/libGLES_mali.so \
	hardware/samsung_slsi/slsiap/prebuilt/library/libGLES_mali.so.64:system/lib64/egl/libGLES_mali.so

PRODUCT_COPY_FILES += \
	hardware/samsung_slsi/slsiap/prebuilt/modules/mali.ko:system/lib/modules/mali.ko
endif

# coda
ifeq ($(strip $(kernel_patch_level)),4)
PRODUCT_COPY_FILES += \
	hardware/samsung_slsi/slsiap/prebuilt/modules/nx_vpu.ko:system/lib/modules/nx_vpu.ko
endif

# optee
ifeq ($(strip $(is_secure)),1)
PRODUCT_COPY_FILES += \
	linux/secureos/optee_os_3.18/optee.ko:system/lib/modules/optee.ko \
	linux/secureos/optee_os_3.18/optee_armtz.ko:system/lib/modules/optee_armtz.ko
endif

# ffmpeg libraries
EN_FFMPEG_EXTRACTOR := false
EN_FFMPEG_AUDIO_DEC := false
#ifeq ($(EN_FFMPEG_EXTRACTOR),true)
#PRODUCT_COPY_FILES += \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavcodec-2.1.4.so:system/lib/libavcodec-2.1.4.so    \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavdevice-2.1.4.so:system/lib/libavdevice-2.1.4.so  \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavfilter-2.1.4.so:system/lib/libavfilter-2.1.4.so  \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavformat-2.1.4.so:system/lib/libavformat-2.1.4.so  \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavresample-2.1.4.so:system/lib/libavresample-2.1.4.so \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libavutil-2.1.4.so:system/lib/libavutil-2.1.4.so      \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libswresample-2.1.4.so:system/lib/libswresample-2.1.4.so \
	#hardware/samsung_slsi/slsiap/omx/codec/ffmpeg/libs/libswscale-2.1.4.so:system/lib/libswscale-2.1.4.so
#endif

# Nexell Dual Audio
EN_DUAL_AUDIO := false
ifeq ($(EN_DUAL_AUDIO),true)
	PRODUCT_COPY_FILES += \
	  	hardware/samsung_slsi/slsiap/prebuilt/libnxdualaudio/lib/libnxdualaudio.so:system/lib/libnxdualaudio.so
endif

# wifi

PRODUCT_COPY_FILES += \
 	device/nexell/s5p6818_drone/modules/wlan.ko:system/lib/modules/wlan.ko 

################################################################################
# generic
################################################################################
#PRODUCT_COPY_FILES += \
  #device/nexell/s5p6818_drone/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
  #frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
  #frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
  #frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
  #frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
  #frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
  #frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
  #frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
  #frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
  #frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
  #linux/slsiap/library/lib/ratecontrol/libnxvidrc_android.so:system/lib/libnxvidrc_android.so

#PRODUCT_COPY_FILES += \
    #frameworks/native/data/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
    #frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    #frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    #frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    #frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    #frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    #frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    #frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    #frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    #frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    #frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    #frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    #frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    #frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    #frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    #frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    #frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    #frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
    #frameworks/native/data/etc/android.hardware.nfc.xml:system/etc/permissions/android.hardware.nfc.xml \
    #frameworks/native/data/etc/android.hardware.nfc.hce.xml:system/etc/permissions/android.hardware.nfc.hce.xml \
    #frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    #frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    #frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml \
    #frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml

PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.barometer.xml:system/etc/permissions/android.hardware.sensor.barometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepcounter.xml:system/etc/permissions/android.hardware.sensor.stepcounter.xml \
    frameworks/native/data/etc/android.hardware.sensor.stepdetector.xml:system/etc/permissions/android.hardware.sensor.stepdetector.xml \
    frameworks/native/data/etc/android.hardware.audio.low_latency.xml:system/etc/permissions/android.hardware.audio.low_latency.xml \
    frameworks/native/data/etc/android.hardware.opengles.aep.xml:system/etc/permissions/android.hardware.opengles.aep.xml \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.ethernet.xml:system/etc/permissions/android.hardware.ethernet.xml

ifeq ($(strip $(target_arch)),32)
PRODUCT_COPY_FILES += \
	linux/platform/s5p6818/library/lib/libnxvidrc_android.so:system/lib/libnxvidrc_android.so
endif

PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/apk/serial_port.apk:system/app/serial_port.apk \
	device/nexell/s5p6818_drone/rpdzkj/apk/GPSTest.apk:system/app/GPSTest.apk \
	device/nexell/s5p6818_drone/rpdzkj/apk/RP_TEST.apk:system/app/RP_TEST.apk \
	device/nexell/s5p6818_drone/rpdzkj/apk/android.audiorecorder.apk:system/app/android.audiorecorder.apk

#	device/nexell/s5p6818_drone/rpdzkj/apk/es4.apk:system/app/es4.apk \
#	device/nexell/s5p6818_drone/rpdzkj/apk/ESTaskManager.apk:system/app/ESTaskManager.apk \

PRODUCT_AAPT_CONFIG := normal large xlarge hdpi xhdpi xxhdpi
#PRODUCT_AAPT_PREF_CONFIG := hdpi
PRODUCT_AAPT_PREF_CONFIG := xhdpi

# 4330 delete nosdcard
# PRODUCT_CHARACTERISTICS := tablet,nosdcard
# PRODUCT_CHARACTERISTICS := tablet,usbstorage
PRODUCT_CHARACTERISTICS := tablet


#diceplayer
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/apk/DicePlayer.apk:system/app/DicePlayer.apk	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_froyo.so:system/lib/libdice_froyo.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_froyo_nvidia.so:system/lib/libdice_froyo_nvidia.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_gingerbread.so:system/lib/libdice_gingerbread.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_honeycomb.so:system/lib/libdice_honeycomb.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_ics.so:system/lib/libdice_ics.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_jb.so:system/lib/libdice_jb.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_jb2.so:system/lib/libdice_jb2.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_kk.so:system/lib/libdice_kk.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_loadlibrary.so:system/lib/libdice_loadlibrary.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_software.so:system/lib/libdice_software.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_software_froyo.so:system/lib/libdice_software_froyo.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libdice_software_kk.so:system/lib/libdice_software_kk.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libffmpeg_dice.so:system/lib/libffmpeg_dice.so	\
	device/nexell/s5p6818_drone/rpdzkj/lib/diceplayer/libsonic.so:system/lib/libsonic.so	
	
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/lib/libserial_port.so:system/lib/libserial_port.so 
	
DEVICE_PACKAGE_OVERLAYS := \
	device/nexell/s5p6818_drone/overlay

ifeq ($(ShowNavigationBar),true)
DEVICE_PACKAGE_OVERLAYS += \
	device/nexell/s5p6818_drone/overlay2
endif

PRODUCT_TAGS += dalvik.gc.type-precise

PRODUCT_PACKAGES += \
    libwpa_client \
    hostapd \
    dhcpcd.conf \
    wpa_supplicant \
    wpa_supplicant.conf

PRODUCT_PACKAGES += \
	LiveWallpapersPicker \
	librs_jni \
	com.android.future.usb.accessory

PRODUCT_PACKAGES += \
	audio.a2dp.default \
	audio.usb.default \
	audio.r_submix.default

# Filesystem management tools
PRODUCT_PACKAGES += \
    e2fsck

ifeq ($(strip $(is_secure)),1)
PRODUCT_PACKAGES += \
	libteec \
	tee-android-supplicant \
	xtest \
	helloworld-optee \
	aes-perf
endif

# Product Property
# common
PRODUCT_PROPERTY_OVERRIDES := \
	wifi.interface=wlan0 \
	ro.rpdzkj.keyguard.enable=false

# set DPI
ifeq ($(strip $(DisplayDPI)),)
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=160
else
PRODUCT_PROPERTY_OVERRIDES += \
    ro.sf.lcd_density=$(DisplayDPI)
endif

# 4330 openl ui property
#PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=131072 \
	ro.hwui.texture_cache_size=72 \
	ro.hwui.layer_cache_size=48 \
	ro.hwui.path_cache_size=16 \
	ro.hwui.shape_cache_size=4 \
	ro.hwui.gradient_cache_size=1 \
	ro.hwui.drop_shadow_cache_size=6 \
	ro.hwui.texture_cache_flush_rate=0.4 \
	ro.hwui.text_small_cache_width=1024 \
	ro.hwui.text_small_cache_height=1024 \
	ro.hwui.text_large_cache_width=2048 \
	ro.hwui.text_large_cache_height=1024 \
	ro.hwui.disable_scissor_opt=true

# setup dalvik vm configs.
$(call inherit-product, frameworks/native/build/tablet-10in-xhdpi-2048-dalvik-heap.mk)
#$(call inherit-product, frameworks/native/build/tablet-7in-hdpi-1024-dalvik-heap.mk)

# The OpenGL ES API level that is natively supported by this device.
# This is a 16.16 fixed point number
PRODUCT_PROPERTY_OVERRIDES += \
	ro.opengles.version=131072

#PRODUCT_PACKAGES += \
	#VolantisLayoutDroneLS5P6818
PRODUCT_PACKAGES += \
	VolantisLayouts5p6818_drone

PRODUCT_PACKAGES += \
	rtw_fwloader

# Enable AAC 5.1 output
#PRODUCT_PROPERTY_OVERRIDES += \
	media.aac_51_output_enabled=true

# set default USB configuration
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
	persist.sys.usb.config=mtp

# ota updater test
#PRODUCT_PACKAGES += \
	#OTAUpdateCenter

# wifi
ifeq ($(BOARD_WIFI_VENDOR),realtek)
#PRODUCT_PACKAGES += rtw_fwloader
$(call inherit-product-if-exists, hardware/realtek/wlan/config/p2p_supplicant.mk)
endif

ifeq ($(BOARD_WIFI_VENDOR),broadcom)
WIFI_BAND := 802_11_BG
$(call inherit-product-if-exists, hardware/broadcom/wlan/bcmdhd/firmware/bcm4329/device-bcm.mk)
endif

# 3G/LTE
################################################################################
# rild
################################################################################
#	chat	\ modify by yangbin 2016-01-04
#	pppd	\
#	reference-ril	\
# changed refer to patch from li by ivy 2015.11.16
PRODUCT_PACKAGES += \
	libril	\
	librilutils	\
	rild \
	Mms \
    	ContactsCommon \
    	Dialer \
    	libril3 \
    	rild3 \
    	Stk
#add by yangbin for EC20,2016-01-04
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/system/bin/pppd:system/bin/pppd \
	device/nexell/s5p6818_drone/rpdzkj/system/bin/chat:system/bin/chat \
	device/nexell/s5p6818_drone/rpdzkj/system/etc/ppp/ip-down:system/etc/ppp/ip-down \
	device/nexell/s5p6818_drone/rpdzkj/system/etc/ppp/ip-up:system/etc/ppp/ip-up \
	device/nexell/s5p6818_drone/rpdzkj/system/lib/libreference-ril.so:system/lib/libreference-ril.so \
	device/nexell/s5p6818_drone/rpdzkj/etc/apns-conf.xml:system/etc/apns-conf.xml

# usb_modeswitch, Huawei rild
$(call inherit-product, device/nexell/s5p6818_drone/rpdzkj/rild/radio_common.mk) 

# call slsiap
$(call inherit-product-if-exists, hardware/samsung_slsi/slsiap/slsiap.mk)

# google gms
ifneq ($(NO_GOOGLE_GMS),true)
ifeq (0,1)
#$(call inherit-product-if-exists, vendor/google/gapps/gapps.mk)
$(call inherit-product-if-exists, vendor/google/products/gms_base.mk)
endif
GAPPS_VARIANT := pico
GAPPS_EXCLUDED_PACKAGES := SetupWizard
GAPPS_EXCLUDED_PACKAGES += GooglePackageInstaller
#GAPPS_EXCLUDED_PACKAGES += FaceLock
#GAPPS_EXCLUDED_PACKAGES += Velvet
GAPPS_FORCE_WEBVIEW_OVERRIDES := true
GAPPS_FORCE_BROWSER_OVERRIDES := true
$(call inherit-product-if-exists, vendor/google/build/opengapps-packages.mk)
endif

# Nexell Application
$(call inherit-product-if-exists, vendor/nexell/apps/nxvideoplayer.mk)
$(call inherit-product-if-exists, vendor/nexell/apps/nxaudioplayer.mk)
$(call inherit-product-if-exists, vendor/nexell/apps/smartsync.mk)

#rpdzkj Realtek add start for mxliao ,at 2015-10-27
$(call inherit-product, hardware/realtek/bt/firmware/rtl8723a/device-rtl.mk) 
$(call inherit-product, hardware/realtek/bt/firmware/rtl8723b/device-rtl.mk) 
$(call inherit-product, hardware/realtek/bt/firmware/rtl8761a/device-rtl.mk) 
$(call inherit-product, hardware/realtek/bt/firmware/rtl8821a/device-rtl.mk) 
#realtek add end

# OEM build setting
$(call inherit-product-if-exists, OEM/build.mk)

