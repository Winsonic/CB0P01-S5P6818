# 3G Data Card Packages
#PRODUCT_PACKAGES += \
#	chat \
#	pppd \
#    rild

# 3G Data Card Configuration Flie
PRODUCT_COPY_FILES += \
	device/nexell/s5p6818_drone/rpdzkj/rild/3g_dongle.cfg:system/etc/3g_dongle.cfg \
	device/nexell/s5p6818_drone/rpdzkj/rild/usb_modeswitch:system/bin/usb_modeswitch \
	device/nexell/s5p6818_drone/rpdzkj/rild/call-pppd:system/xbin/call-pppd \
	device/nexell/s5p6818_drone/rpdzkj/rild/usb_modeswitch.sh:system/xbin/usb_modeswitch.sh \
	device/nexell/s5p6818_drone/rpdzkj/rild/lib/libhuawei-ril.so:system/lib/libhuawei-ril.so \

#	device/nexell/s5p6818_drone/rpdzkj/rild/ip-down:system/etc/ppp/ip-down \
#	device/nexell/s5p6818_drone/rpdzkj/rild/ip-up:system/etc/ppp/ip-up \
#	device/nexell/s5p6818_drone/rpdzkj/rild/apns-conf_sdk.xml:system/etc/apns-conf.xml \


# 3G Data Card usb modeswitch File
PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,device/nexell/s5p6818_drone/rpdzkj/rild/usb_modeswitch.d,system/etc/usb_modeswitch.d)

# Radio parameter
PRODUCT_PROPERTY_OVERRIDES += \
	ro.sw.embeded.telephony=true

#	rild.libargs=-d/dev/ttyUSB2 \
#	rild.libpath=/system/lib/libhuawei-ril.so \



