#!/system/bin/sh

original_hdmi_res=$(/system/bin/getprop persist.hwc.resolution)

if [ ${original_hdmi_res} == "18" ]; then
	setprop persist.hwc.resolution 8
elif [ ${original_hdmi_res} == "8" ]; then
	setprop persist.hwc.resolution 2
elif [ ${original_hdmi_res} == "2" ]; then
	setprop persist.hwc.resolution 19
elif [ ${original_hdmi_res} == "19" ]; then
	setprop persist.hwc.resolution 2
else
	setprop persist.hwc.resolution 18
fi


sleep 3


setprop persist.hwc.resolution $original_hdmi_res

