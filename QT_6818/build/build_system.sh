#bin/sh

echo "package system ...."

TOP=`pwd`
top=$TOP
echo "top: $top"

result_dir=$top/result
BOARD_NAME=rp6818

function make_ext4()
{
    local board_name=$1
    local partition_name=$2
    local partition_size=

    if [ ! -z $3 ]; then
	partition_size=$3
    else
 	partition_size=300000
    fi

    cd $result_dir
    local host_out_dir="${top}/build/host/linux-x86/bin"
  #  PATH=${host_out_dir}/bin:$PATH \
  #      && mkuserimg.sh -s ${result_dir}/${partition_name} ${result_dir}/${partition_name}.img ext4 ${partition_name} ${partition_size}

    $host_out_dir/mk_image.sh -r system -s $partition_size -f ext4 -n system.img

	echo "result_dir: $result_dir"
	echo "partition_name: $partition_name"
	echo "partition_size: $partition_size"
	echo "PATH: $PATH"
}

make_ext4 $BOARD_NAME system

cd $top


echo "package system end !!!"
