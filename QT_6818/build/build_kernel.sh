#/bin/sh

echo "build kernel..."

TOP=`pwd`
result_dir=$TOP/result

BOARD_NAME=s5p6818_drone
toolchain_version=4.8
top=`pwd`
cpu_num=12

echo "top : $top"


    if [ ! -d $top/build/prebuilts/gcc/linux-x86/arm/arm-eabi-$toolchain_version/bin ]; then
        echo "Error: can't find android toolchain!!!"
        echo "Check android source"
        exit 1
    fi

    #echo "PATH setting for android toolchain"
    export PATH=${top}/build/prebuilts/gcc/linux-x86/arm/arm-eabi-$toolchain_version/bin/:$PATH
    arm-eabi-gcc -v
    if [ $? -ne 0 ]; then
        echo "Error: can't check arm-eabi-gcc"
        echo "Check android source"
        exit 1
    fi

    echo "toolchain_version: $toolchain_version"

# make ext4 image by android tool 'mkuserimg.sh'
# arg1 : board name
# arg2 : partition name
# option arg3 : partition size
function make_ext4()
{
    local board_name=${1}
    local partition_name=${2}
    local partition_size=

    if [ ! -z $3 ]; then
        partition_size=${3}
    else
        partition_size=67108864
    fi

    local host_out_dir="${top}/build/host/linux-x86"
    PATH=${host_out_dir}/bin:$PATH \
        && mkuserimg.sh -s ${result_dir}/${partition_name} ${result_dir}/${partition_name}.img ext4 ${partition_name} ${partition_size}

	echo "result_dir: $result_dir"
	echo "partition_name: $partition_name"
	echo "partition_size: $partition_size"
	echo "PATH: $PATH"

}
 


cd $top/kernel
#make clean

#if [ ! -f $top/kernel/.config ]; then
#	cp $top/kernel/arch/arm/configs/${BOARD_NAME}_android_lollipop_defconfig ./config
#fi

rm -f $top/kernel/arch/arm/boot/uImage
make ARCH=arm uImage -j$cpu_num

	echo "top: $top ; result_dir:$result_dir"
if [ ! -f $top/kernel/arch/arm/boot/uImage ]; then
	echo "building kernel failed !"
	exit 1
fi

	cp $top/kernel/arch/arm/boot/uImage $result_dir/boot

make_ext4 $BOARD_NAME boot

cd $top

echo "build kernel end"
