#include <linux/videodev2.h>
#include <nxp-v4l2.h>
#include "RP5065.h"

using namespace android;

enum {
    WB_INCANDESCENT = 0,
    WB_FLUORESCENT,
    WB_DAYLIGHT,
    WB_CLOUDY,
    WB_TUNGSTEN,
    WB_AUTO,
    WB_MAX 
};

enum {
    COLORFX_NONE = 0,
    COLORFX_MONO,
    COLORFX_SEPIA,
    COLORFX_NEGATIVE,
    COLORFX_MAX
};

#define MIN_EXPOSURE     0
#define MAX_EXPOSURE     6

#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))

const int32_t ResolutionRP5065[] = {
#if 0
    // 648, 480,
	  608,479,
   2560,1920,	// 5M
   1568, 1176,      // 2M capture and video 
   //1560, 1170, // can capture ,but bad quality
   //1280,720,	// can not video
  //1024, 768,	// can not video
    608,479,		// VGA
 #else
 	2592, 1944,
 	1600, 1200,
 	1280, 720,
	640, 480,
 #endif
};

// TOP/system/media/camera/include/system/camera_metadata_tags.h
static const uint8_t AvailableAfModesRP5065[] = {
    ANDROID_CONTROL_AF_MODE_OFF,
    ANDROID_CONTROL_AF_MODE_AUTO,
    ANDROID_CONTROL_AF_MODE_MACRO,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_VIDEO
};

static const uint8_t AvailableAeModesRP5065[] = {
    ANDROID_CONTROL_AE_MODE_OFF,
    ANDROID_CONTROL_AE_MODE_ON,
    ANDROID_CONTROL_AE_MODE_ON_AUTO_FLASH
};

static const uint8_t SceneModeOverridesRP5065[] = {
    // ANDROID_CONTROL_SCENE_MODE_PORTRAIT
    ANDROID_CONTROL_AE_MODE_ON,
    ANDROID_CONTROL_AWB_MODE_AUTO,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
    // ANDROID_CONTROL_SCENE_MODE_LANDSCAPE
    ANDROID_CONTROL_AE_MODE_ON,
    ANDROID_CONTROL_AWB_MODE_AUTO,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
    // ANDROID_CONTROL_SCENE_MODE_SPORTS
    ANDROID_CONTROL_AE_MODE_ON,
    ANDROID_CONTROL_AWB_MODE_DAYLIGHT,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
    // ANDROID_CONTROL_SCENE_MODE_NIGHT
#if 0
    ANDROID_CONTROL_AE_MODE_ON_AUTO_FLASH,
    ANDROID_CONTROL_AWB_MODE_AUTO,
    ANDROID_CONTROL_AF_MODE_CONTINUOUS_PICTURE
#endif
};

static const uint8_t AvailableEffectsRP5065[] = {
    ANDROID_CONTROL_EFFECT_MODE_MONO,
    ANDROID_CONTROL_EFFECT_MODE_NEGATIVE,
    ANDROID_CONTROL_EFFECT_MODE_SEPIA,
    ANDROID_CONTROL_EFFECT_MODE_AQUA
};

static const uint8_t AvailableSceneModesRP5065[] = {
    ANDROID_CONTROL_SCENE_MODE_PORTRAIT,
    ANDROID_CONTROL_SCENE_MODE_LANDSCAPE,
    ANDROID_CONTROL_SCENE_MODE_SPORTS,
#if 0
    ANDROID_CONTROL_SCENE_MODE_NIGHT
#endif
};

static const int32_t AvailableFpsRangesRP5065[] = {
    8, 15
};

static const uint32_t ExposureCompensationRangeRP5065[] = {
    // MIN_EXPOSURE, MAX_EXPOSURE
    -3, 3
};

static const uint8_t AvailableAntibandingModesRP5065[] = {
    ANDROID_CONTROL_AE_ANTIBANDING_MODE_OFF,
    ANDROID_CONTROL_AE_ANTIBANDING_MODE_50HZ,
    ANDROID_CONTROL_AE_ANTIBANDING_MODE_60HZ
};

static const uint8_t AvailableAwbModesRP5065[] = {
    ANDROID_CONTROL_AWB_MODE_OFF,
    ANDROID_CONTROL_AWB_MODE_AUTO,
    ANDROID_CONTROL_AWB_MODE_DAYLIGHT,
    ANDROID_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT,
    ANDROID_CONTROL_AWB_MODE_FLUORESCENT,
    ANDROID_CONTROL_AWB_MODE_INCANDESCENT
};

void RP5065::init()
{
    // TODO
    Width = ResolutionRP5065[0];
    Height = ResolutionRP5065[1];
    NumResolutions = ARRAY_SIZE(ResolutionRP5065)/2;
    Resolutions = ResolutionRP5065;
    NumAvailableAfModes = ARRAY_SIZE(AvailableAfModesRP5065);
    AvailableAfModes = AvailableAfModesRP5065;
    NumAvailableAeModes = ARRAY_SIZE(AvailableAeModesRP5065);
    AvailableAeModes = AvailableAeModesRP5065;
    NumSceneModeOverrides = ARRAY_SIZE(SceneModeOverridesRP5065);
    SceneModeOverrides = SceneModeOverridesRP5065;
    ExposureCompensationRange = ExposureCompensationRangeRP5065;
    AvailableAntibandingModes = AvailableAntibandingModesRP5065;
    NumAvailAntibandingModes = ARRAY_SIZE(AvailableAntibandingModesRP5065);
    AvailableAwbModes = AvailableAwbModesRP5065;
    NumAvailAwbModes = ARRAY_SIZE(AvailableAwbModesRP5065);
    AvailableEffects = AvailableEffectsRP5065;
    NumAvailEffects = ARRAY_SIZE(AvailableEffectsRP5065);
    AvailableSceneModes = AvailableSceneModesRP5065;
    NumAvailSceneModes = ARRAY_SIZE(AvailableSceneModesRP5065);
    AvailableFpsRanges = AvailableFpsRangesRP5065;
    NumAvailableFpsRanges = ARRAY_SIZE(AvailableFpsRangesRP5065);

    // TODO
    FocalLength = 3.43f;
    Aperture = 2.7f;
    MinFocusDistance = 0.1f;
    FNumber = 2.7f;

    // TODO
    //MaxFaceCount = 16;
    MaxFaceCount = 1;

    // Crop
    CropWidth = Width;
    CropHeight = Height;
    CropLeft = 0;
    CropTop = 0;
}

RP5065::RP5065()
{
    init();
}

RP5065::RP5065(uint32_t v4l2ID)
    : NXCameraBoardSensor(v4l2ID)
{
    init();
}

RP5065::~RP5065()
{
}
void RP5065::setAfMode(uint8_t afMode)
{
    AfMode = afMode;
}

void RP5065::afEnable(bool enable)
{
}

void RP5065::setEffectMode(uint8_t effectMode)
{
    if (effectMode != EffectMode) {
        uint32_t val = 0;

        switch (effectMode) {
        case ANDROID_CONTROL_EFFECT_MODE_OFF:
            val = COLORFX_NONE;
            break;
        case ANDROID_CONTROL_EFFECT_MODE_MONO:
            val = COLORFX_MONO;
            break;
        case ANDROID_CONTROL_EFFECT_MODE_NEGATIVE:
            val = COLORFX_NEGATIVE;
            break;
        case ANDROID_CONTROL_EFFECT_MODE_SEPIA:
            val = COLORFX_SEPIA;
            break;
        default:
            //ALOGE("%s: unsupported effectmode 0x%x", __func__, effectMode);
            return;
        }

        v4l2_set_ctrl(V4l2ID, V4L2_CID_COLORFX, val);
        EffectMode = effectMode;
    }
}

void RP5065::setSceneMode(uint8_t sceneMode)
{
}

void RP5065::setAntibandingMode(uint8_t antibandingMode)
{
}

void RP5065::setAwbMode(uint8_t awbMode)
{
    if (awbMode != AwbMode) {
        uint32_t val = 0;

        switch (awbMode) {
        case ANDROID_CONTROL_AWB_MODE_OFF:
            v4l2_set_ctrl(V4l2ID, V4L2_CID_AUTO_WHITE_BALANCE, 0);
            return;
        case ANDROID_CONTROL_AWB_MODE_AUTO:
            v4l2_set_ctrl(V4l2ID, V4L2_CID_AUTO_WHITE_BALANCE, 1);
            return;
        case ANDROID_CONTROL_AWB_MODE_DAYLIGHT:
            val = WB_DAYLIGHT;
            break;
        case ANDROID_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT:
            val = WB_DAYLIGHT;
            break;
        case ANDROID_CONTROL_AWB_MODE_FLUORESCENT:
            val = WB_FLUORESCENT;
            break;
        case ANDROID_CONTROL_AWB_MODE_INCANDESCENT:
            val = WB_INCANDESCENT;
            break;
        default:
            //ALOGE("%s: unsupported awb mode 0x%x", __func__, awbMode);
            return;
        }
        AwbMode = awbMode;

        v4l2_set_ctrl(V4l2ID, V4L2_CID_WHITE_BALANCE_TEMPERATURE, val);
    }
}

void RP5065::setExposure(int32_t exposure)
{
    if (exposure < MIN_EXPOSURE || exposure > MAX_EXPOSURE) {
        ALOGE("%s: invalid exposure %d", __func__, exposure);
        return;
    }

    if (exposure != Exposure) {
        Exposure = exposure;
        v4l2_set_ctrl(V4l2ID, V4L2_CID_BRIGHTNESS, exposure);
    }
}

uint32_t RP5065::getZoomFactor(void)
{
    // disable zoom
    return 1;
}

status_t RP5065::setZoomCrop(uint32_t left, uint32_t top, uint32_t width, uint32_t height)
{
    return NO_ERROR;
}

int RP5065::setFormat(int width, int height, int format)
{
    int sensorWidth, sensorHeight;
#if 1	
    if (width == 608)
        sensorWidth = 640;
   else if(width == 2560)
   	sensorWidth = 2592;
   else if(width == 1024)
   	sensorWidth = 1280;
   else if(width == 1568 || width == 1560)
   	sensorWidth = 1600;
    else {
        ALOGE("%s: invalid width %d", __func__, width);
      //  return -EINVAL;
      	sensorWidth = width;
    }
	
    if (height == 479)
        sensorHeight = 480;
    else if(height == 1920)
		sensorHeight = 1944;
    else if (height == 1176 || height == 1170)
		sensorHeight = 1200;
    else if(height == 768)
		sensorHeight = 720;
    else {
        ALOGE("%s: invalid height %d", __func__, height);
    //    return -EINVAL;
    	sensorHeight = height;
    }
#endif 

//	sensorWidth = width;
//	sensorHeight = height;
	ALOGE(" %s   width: %d , height: %d",__func__,sensorWidth,sensorHeight);
	
    return v4l2_set_format(V4l2ID, sensorWidth, sensorHeight, format);
}
