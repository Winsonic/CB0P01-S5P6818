#!/bin/bash
#

trap error_handling ERR

error_handling()
{
        error_code=$?

        if [ "$1" != "" ]; then
                error_code=$1
        fi

        echo Error!!! \(code=$error_code\)
        exit $error_code
}

./1_buildLinuxKernel.sh
./2_buildAndroid.sh
./3_buildOTA.sh

