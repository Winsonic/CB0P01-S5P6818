#!/bin/bash

trap error_handling ERR

function gettop
{
    local TOPFILE=build/core/envsetup.mk
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ] ; then
        echo $TOP
    else
        if [ -f $TOPFILE ] ; then
            # The following circumlocution (repeated below as well) ensures
            # that we record the true directory name and not one that is
            # faked up with symlink names.
            PWD= /bin/pwd
        else
            # We redirect cd to /dev/null in case it's aliased to
            # a command that prints something as a side-effect
            # (like pushd)
            local HERE=$PWD
            T=
            while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
                cd .. > /dev/null
                T=`PWD= /bin/pwd`
            done
            cd $HERE > /dev/null
            if [ -f "$T/$TOPFILE" ]; then
                echo $T
            fi
        fi
    fi
}

error_handling()
{
        error_code=$?

        if [ "$1" != "" ]; then
                error_code=$1
        fi

        echo Error!!! \(code=$error_code\)
        exit $error_code
}

TOP=$(gettop)
if [ "$TOP" == "" ]; then
    echo "Couldn't locate the top of the tree.  Try setting TOP." >&2
    return
fi

# Read ReleasePlan.txt
. $TOP/build_scripts/common/ReadReleasePlan.sh
. $TOP/build_scripts/common/SetHWPlatform.sh

if [ ! "$RKParameter_Launch" ]; then
    echo "Can not find proper HW Platform for $HWPlatform."
    return
fi


# Set Environment
cd $TOP
. build/envsetup.sh
lunch $RKParameter_Launch


# Build BootLoader
echo Build BootLoader
./device/nexell/tools/build.sh -b $RKParameter_BoardName -a $RKParameter_ARM_ARCH -t u-boot

# Build QT BootLoader
echo Build QT BootLoader
cd $TOP/QT_6818
./build/build_uboot.sh

# Build QT Kernel
echo
echo Build QT Kernel
cd $TOP/QT_6818/kernel
make $RKParameter_QT_Kernel
touch .scmversion

#if [ "$HAS_VIDEO_LOGO" == "Y" ]; then
#scripts/config --enable CONFIG_$BOOTLOGO_NAME
#make oldnoconfig
#fi

cd $TOP/QT_6818
./build/build_kernel.sh


echo ${OEM_ID}-${Product_ID}-01QT > ./fw_config
if [ -f "./fw_config" ];then
    W_Version=`cat ./fw_config`
    if [ "$W_Version" != "" ];then
        W_Version="$W_Version"".""$RKParameter_LCD_Resolution""-"
    fi
#    now_time=`date +%Y%m%d`
    if [ "$CI_BUILD_REF" != "" ];then
    	last_commit=$(echo $CI_BUILD_REF | cut -c -7)
    else
	last_commit=`git rev-parse --short --verify master`
    fi
    export W_Firmware="$W_Version"$last_commit-${Version}
    echo $W_Firmware
fi


# Zip SD card images folder
echo Zip SD card images folder
cp -v sd-update/$LCD_TXT sd-update/rp6818/lcd.txt
cp -v result/u-boot.bin sd-update/rp6818/u-boot.bin
cp -v result/boot.img sd-update/rp6818/boot.img
cp -v result/system.img sd-update/rp6818/system.img
cd $TOP/QT_6818/sd-update/
zip -r $TOP/$W_Firmware.zip rp6818/
cd $TOP
md5sum $W_Firmware.zip > $W_Firmware.md5

# for GitLab CI
if [ "$CI_BUILD_REF" != "" ];then
    if [ "$SSHPASS" != "" ];then
	yes | pscp -pw $SSHPASS $W_Firmware.zip winsonic@192.168.1.12:GitLab-CI/Release/CB0P01-S5P6818/
	yes | pscp -pw $SSHPASS $W_Firmware.md5 winsonic@192.168.1.12:GitLab-CI/Release/CB0P01-S5P6818/
    fi
fi

# beep sound
echo $'\07'
