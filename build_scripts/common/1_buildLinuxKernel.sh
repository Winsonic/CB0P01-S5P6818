#!/bin/bash

trap error_handling ERR

function gettop
{
    local TOPFILE=build/core/envsetup.mk
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ] ; then
        echo $TOP
    else
        if [ -f $TOPFILE ] ; then
            # The following circumlocution (repeated below as well) ensures
            # that we record the true directory name and not one that is
            # faked up with symlink names.
            PWD= /bin/pwd
        else
            # We redirect cd to /dev/null in case it's aliased to
            # a command that prints something as a side-effect
            # (like pushd)
            local HERE=$PWD
            T=
            while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
                cd .. > /dev/null
                T=`PWD= /bin/pwd`
            done
            cd $HERE > /dev/null
            if [ -f "$T/$TOPFILE" ]; then
                echo $T
            fi
        fi
    fi
}

error_handling()
{
        error_code=$?

        if [ "$1" != "" ]; then
                error_code=$1
        fi

        echo Error!!! \(code=$error_code\)
        exit $error_code
}

TOP=$(gettop)
if [ "$TOP" == "" ]; then
    echo "Couldn't locate the top of the tree.  Try setting TOP." >&2
    return
fi

# Read ReleasePlan.txt
. $TOP/build_scripts/common/ReadReleasePlan.sh
. $TOP/build_scripts/common/SetHWPlatform.sh

if [ ! "$RKParameter_Launch" ]; then
    echo Can not find proper HW Platform for $HWPlatform.
    return
fi


# Set Environment
cd $TOP
. build/envsetup.sh
lunch $RKParameter_Launch


# Build BootLoader
echo Build BootLoader
./device/nexell/tools/build.sh -b $RKParameter_BoardName -a $RKParameter_ARM_ARCH -t u-boot

# Build Kernel
echo
echo Build Kernel
cd $TOP/kernel
make $RKParameter_Kernel
touch .scmversion

if [ "$BL_PWM_NON_REVERSE" == "Y" ]; then
scripts/config --enable CONFIG_BACKLIGHT_PWM_NON_REVERSE
make oldnoconfig
fi

#if [ "$HAS_VIDEO_LOGO" == "Y" ]; then
#scripts/config --enable CONFIG_$BOOTLOGO_NAME
#make oldnoconfig
#fi

cd $TOP
./device/nexell/tools/build.sh -b $RKParameter_BoardName -a $RKParameter_ARM_ARCH -t kernel


# beep sound
echo $'\07'
