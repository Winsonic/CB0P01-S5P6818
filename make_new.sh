#! /bin/bash

build_uboot=false
build_kernel=false
build_android=false
build_module=false
build_modules=false
build_dist=false

copy_uboot=false
copy_kernel=false
copy_android=false
copy_cache=false
copy_userdata=false
copy_all=false

lcd_size=default

board=default

function usage()
{
	echo "  usage:"

	echo " ./make.sh -t u-boot -l 1024 -c u-boot -b rp6818//build rp6818 u-boot.bin, lcd is 1024 and then copy u-boot.bin to rp-dev cdrom"
	echo "-t to build image"
		echo "-t u-boot/kernel/android/modlue/modules/dist, you can choose one of them, or more then one"
		echo "seperate them with a space, for example -t u-boot kernel"	

	echo "-l to choose lcd"
		echo "-l 71024/1024/1280/1920, you can just choose one of them, cause the source code only support one type of lcd at the same time"

	echo "-c to copy image"
		echo "-c u-boot.bin/boot.img/system.img/cache.img/userdata.img, you can choose one of them, or more then one"
		echo "seperate them with a space, for example -t u-boot.bin boot.img"

	echo "-b to select board"
		echo "-c rp6818/pro6818, you can choose one of them, or use default board"
		echo "seperate them with a space, for example -b rp6818"
			
	
}

function parse_args()
{
	while [ $# -ge 1 ]; do
		case "$1" in
			-t)	shift 1
				while [ $1 != "" ] && [ $1 != "-l"  && $1 != "-c" ]; do
					case "$1" in
						u-boot ) build_uboot=true;;
						kernel ) build_kernel=true;;
						android) build_android=true;;
						module ) build_module=true;;
						modules) build_modules=true;;
						dist   ) build_dist=true;;				
					esac
					shift
					if [  $1 == "" ]; then
						break 2
					elif [  $1 == "-l" ] || [  $1 == "-c" ]
						break
					fi
				done ;;
				
			-l)	shift 1
				lcd_size=$1
				shift 1;;

			-c)	shift 1
				while [ $1 != "" ] && [ $1 != "-l"  && $1 != "-c" ]; do
					case "$1" in
						u-boot  ) copy_uboot=true;;
						kernel  ) copy_kernel=true;;
						android ) copy_android=true;;
						cache   ) copy_cache=true;;
						userdata) copy_userdata=true;;
						all     ) copy_all=true; break;;					
					esac
					shift
					if [  $1 == "" ]; then
						break 2
					elif [  $1 == "-l" ] || [  $1 == "-c" ]; then
						break
					fi
				done ;;
			-b)	shift 1
				if [ $1 == "rp6818" ]; then 
					board=rp6818
				elif [ $1 == pro6818 ]; then
					board=pro6818
				fi
				shift
				;;

			-help) usage; exit ;;
				
			*) echo -n "wrong usage"; usage; exit 1;;
		esac
	done
}

function build_images()
{
	if [ $build_modules == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t modules
		echo "build modules successfully"
		return
	fi

	if [ $build_dist == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t dist
		echo "build dist successfully"
		return
	fi

	if [ $build_uboot == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t u-boot
	fi

	if [ $build_kernel == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t kernel
	fi

	if [ $build_android == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t android
	fi

	if [ $build_module == "true" ]; then 
		./device/nexell/tools/build.sh -b s5p6818_drone -t module
	fi
}

function change_lcd_size()
{
	if [ $lcd_size == "default" ]; then
		return
	elif [ $lcd_size == "71024" ]; then
		# fixme cp uboot/board/s5p6818/drone/include/cfg_main_71024.h uboot/board/s5p6818/drone/include/cfg_main.h
		# cp kernel/config_71024 kernel/.config
	elif [ $lcd_size == "1024" ]; then
		#fixme
	elif [ $lcd_size == "1280" ]; then
		#fixme
	elif [ $lcd_size == "1920" ]; then
		#fixme
	fi
}

destination_dir=
function copy_image()
{
	if [ $copy_all == "true" ]; then 
		#fixme cp u-boot.bin boot.img system.img cache.img userdata.img $destination_dir
		echo "build modules successfully"
		return
	fi

	if [ $copy_uboot == "true" ]; then 
		#fixme cp u-boot.bin $destination_dir
	fi

	if [ $copy_kernel == "true" ]; then 
		#fixme cp boot.img $destination_dir
	fi

	if [ $copy_android == "true" ]; then 
		#fixme cp system.img $destination_dir
	fi

	if [ $copy_cache == "true" ]; then 
		#fixme cp cache.img $destination_dir
	fi

	if [ $copy_userdata == "true" ]; then 
		#fixme cp userdata.img $destination_dir
	fi
}

parse_args $@
change_lcd_size
build_images
copy_image




