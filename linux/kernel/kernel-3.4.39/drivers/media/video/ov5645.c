
/*
 * ov5645 Camera Driver
 *
 * Copyright (C) 2011 Actions Semiconductor Co.,LTD
 * Wang Xin <wangxin@actions-semi.com>
 *
 * Based on ov227x driver
 *
 * Copyright (C) 2008 Renesas Solutions Corp.
 * Kuninori Morimoto <morimoto.kuninori@renesas.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * fixed by swpark@nexell.co.kr for compatibility with general v4l2 layer (not using soc camera interface)
 */


#include <linux/init.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <media/v4l2-chip-ident.h>
#include <media/v4l2-device.h>
#include <media/v4l2-subdev.h>
#include <media/v4l2-ctrls.h>
#include <linux/delay.h>
#include "ov5645.h"

#define MODULE_NAME "OV5645"


#ifdef OV5645_DEBUG
#define assert(expr) \
    if (unlikely(!(expr))) {				\
        pr_err("Assertion failed! %s,%s,%s,line=%d\n",	\
#expr, __FILE__, __func__, __LINE__);	\
    }

#define OV5645_DEBUG(fmt,args...) printk(KERN_ALERT fmt, ##args)
#else

#define assert(expr) do {} while (0)

#define OV5645_DEBUG(fmt,args...)
#endif

#define PID                 0x02 /* Product ID Number  *///caichsh
#define OV5645              0x53
#define OUTTO_SENSO_CLOCK   24000000
#define NUM_CTRLS           11
#define V4L2_IDENT_OV5645   64188

/* private ctrls */
#define V4L2_CID_SCENE_EXPOSURE         (V4L2_CTRL_CLASS_CAMERA | 0x1001)
#define V4L2_CID_PRIVATE_PREV_CAPT      (V4L2_CTRL_CLASS_CAMERA | 0x1002)


#define V4L2_CID_PRIVATE_FOCUS_RESULT      (V4L2_CTRL_CLASS_CAMERA | 0x1003)
#define V4L2_CID_PRIVATE_FOCUS_MODE      (V4L2_CTRL_CLASS_CAMERA | 0x1004)
#define V4L2_CID_PRIVATE_FOCUS_POSITION_X      (V4L2_CTRL_CLASS_CAMERA | 0x1005)
#define V4L2_CID_PRIVATE_FOCUS_POSITION_Y      (V4L2_CTRL_CLASS_CAMERA | 0x1006)

#if 0
enum {
    V4L2_WHITE_BALANCE_INCANDESCENT = 0,
    V4L2_WHITE_BALANCE_FLUORESCENT,
    V4L2_WHITE_BALANCE_DAYLIGHT,
    V4L2_WHITE_BALANCE_CLOUDY_DAYLIGHT,
    V4L2_WHITE_BALANCE_TUNGSTEN
};
#else
enum {
    V4L2_WHITE_BALANCE_INCANDESCENT = 0,
    /*V4L2_WHITE_BALANCE_FLUORESCENT,*/
    V4L2_WHITE_BALANCE_DAYLIGHT,
    V4L2_WHITE_BALANCE_CLOUDY_DAYLIGHT,
    /*V4L2_WHITE_BALANCE_TUNGSTEN*/
};
#endif


enum {
	SENSOR_AF_ERROR=0,
	SENSOR_AF_FOCUSING,
	SENSOR_AF_FOCUSED,
	SENSOR_AF_SCENE_DETECTING,
	SENSOR_AF_IDLE,
};

enum v4l2_focusmode {
	FOCUS_MODE_AUTO = 0,
	FOCUS_MODE_MACRO,
	FOCUS_MODE_FACEDETECT,
	FOCUS_MODE_INFINITY,
	FOCUS_MODE_CONTINOUS,
	FOCUS_MODE_CONTINOUS_XY,
	FOCUS_MODE_TOUCH,
	FOCUS_MODE_TOUCH_CONTINUOUS,
	FOCUS_MODE_TOUCH_FLASH_AUTO,
	FOCUS_MODE_TOUCH_FLASH_ON,
	FOCUS_MODE_TOUCH_FLASH_OFF,
	FOCUS_MODE_MAX,
};

static int ov5645_video_probe(struct i2c_client *client);
/****************************************************************************************
 * predefined reg values
 */
//#define ARRAY_END { 0xffff, 0xff }
//#define DELAYMARKER { 0xfffe, 0xff }


static struct regval_list ov5645_fmt_yuv422_yuyv[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_fmt_yuv422_yvyu[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_fmt_yuv422_vyuy[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_fmt_yuv422_uyvy[] =
{
    ARRAY_END,
};


/*
 *AWB
 */
static const struct regval_list ov5645_awb_regs_enable[] =
{
    ARRAY_END,
};

static const struct regval_list ov5645_awb_regs_diable[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_wb_cloud_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_wb_daylight_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_wb_incandescence_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_wb_fluorescent_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_wb_tungsten_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_colorfx_none_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_colorfx_bw_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_colorfx_sepia_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_colorfx_negative_regs[] =
{
    ARRAY_END,
};

static struct regval_list ov5645_whitebance_auto[] __attribute__((unused)) =
{
	ARRAY_END,
};

static struct regval_list ov5645_whitebance_cloudy[] __attribute__((unused)) =
{
	ARRAY_END,
};

static  struct regval_list ov5645_whitebance_sunny[] __attribute__((unused)) =
{
	ARRAY_END,
};

static  struct regval_list ov5645_whitebance_fluorescent[] __attribute__((unused)) =
{
	ARRAY_END,

};
static  struct regval_list ov5645_whitebance_incandescent[] __attribute__((unused)) =
{
	ARRAY_END,
};


static  struct regval_list ov5645_effect_normal[] __attribute__((unused)) =
{
    ARRAY_END,
};

static  struct regval_list ov5645_effect_white_black[] __attribute__((unused)) =
{
  ARRAY_END,
};

/* Effect */
static  struct regval_list ov5645_effect_negative[] __attribute__((unused)) =
{
	ARRAY_END,
};
/*¸´¹ÅÐ§¹û*/
static  struct regval_list ov5645_effect_antique[] __attribute__((unused)) =
{
	ARRAY_END,
};

/* Scene */
static  struct regval_list ov5645_scene_auto[] __attribute__((unused)) =
{
	ARRAY_END,
};

static  struct regval_list ov5645_scene_night[] __attribute__((unused)) =
{
	ARRAY_END,
};


/****************************************************************************************
 * structures
 */
struct ov5645_win_size {
    char                        *name;
    __u32                       width;
    __u32                       height;
    __u32                       exposure_line_width;
    __u32                       capture_maximum_shutter;
    const struct regval_list    *win_regs;
    const struct regval_list    *lsc_regs;
    unsigned int                *frame_rate_array;
};

typedef struct {
    unsigned int max_shutter;
    unsigned int shutter;
    unsigned int gain;
    unsigned int dummy_line;
    unsigned int dummy_pixel;
    unsigned int extra_line;
} exposure_param_t;

enum prev_capt {
    PREVIEW_MODE = 0,
    CAPTURE_MODE
};

struct ov5645_priv {
    struct v4l2_subdev                  subdev;
    struct media_pad                    pad;
    struct v4l2_ctrl_handler            hdl;
    const struct ov5645_color_format    *cfmt;
    const struct ov5645_win_size        *win;
    int                                 model;
    bool                                initialized;
    
	int fx;
	int fy;
    /**
     * ctrls
    */
    /* standard */
	struct v4l2_ctrl *auto_focus;
	struct v4l2_ctrl *auto_white_balance;
	struct v4l2_ctrl *exposure;
    struct v4l2_ctrl *white_balance_temperature;
	 /* menu */
    struct v4l2_ctrl *colorfx;
    struct v4l2_ctrl *exposure_auto;
    struct v4l2_ctrl *brightness;
    struct v4l2_ctrl *contrast;
    
    
    struct v4l2_ctrl *gain;
    struct v4l2_ctrl *hflip;
   
    
	
    /* custom */
    struct v4l2_ctrl *scene_exposure;
    struct v4l2_ctrl *prev_capt;
    struct v4l2_ctrl *focus_result;
    struct v4l2_ctrl *focus_mode;
    struct v4l2_ctrl *focus_x;
    struct v4l2_ctrl *focus_y;
	
    struct v4l2_rect rect; /* Sensor window */
    struct v4l2_fract timeperframe;
    enum prev_capt prev_capt_mode;
    exposure_param_t preview_exposure_param;
    exposure_param_t capture_exposure_param;
	
	enum v4l2_focusmode focusmode;
    struct delayed_work delay_work;
	
};

struct ov5645_color_format {
    enum v4l2_mbus_pixelcode code;
    enum v4l2_colorspace colorspace;
};

/****************************************************************************************
 * tables
 */
static const struct ov5645_color_format ov5645_cfmts[] = {
    {
        .code		= V4L2_MBUS_FMT_YUYV8_2X8,
        .colorspace	= V4L2_COLORSPACE_JPEG,
    },
    {
        .code		= V4L2_MBUS_FMT_UYVY8_2X8,
        .colorspace	= V4L2_COLORSPACE_JPEG,
    },
    {
        .code		= V4L2_MBUS_FMT_YVYU8_2X8,
        .colorspace	= V4L2_COLORSPACE_JPEG,
    },
    {
        .code		= V4L2_MBUS_FMT_VYUY8_2X8,
        .colorspace	= V4L2_COLORSPACE_JPEG,
    },
};

/*
 * window size list
 */
#define VGA_WIDTH           640
#define VGA_HEIGHT          480
#define UXGA_WIDTH          1600
#define UXGA_HEIGHT         1200
#define SVGA_WIDTH          1280
#define SVGA_HEIGHT         960
#define OV5645_MAX_WIDTH    UXGA_WIDTH
#define OV5645_MAX_HEIGHT   UXGA_HEIGHT
#define AHEAD_LINE_NUM		15    //10ÐÐ = 50´ÎÑ­»·(OV5645)
#define DROP_NUM_CAPTURE			0
#define DROP_NUM_PREVIEW			0

static unsigned int frame_rate_svga[] = {15,};
static unsigned int frame_rate_uxga[] = {15,15,};



/* 2592x1944*/
static const struct ov5645_win_size OV5645_win_2592x1944 = {
    .name     = "2592x1944",
    .width    = 2592,
    .height   = 1944,
    .win_regs = OV5645_res_2592x1944,
    .frame_rate_array = frame_rate_svga,
};

/* 1280x960 */
static const struct ov5645_win_size OV5645_win_1280x960 = {
    .name     = "SVGA",
    .width    = SVGA_WIDTH,
    .height   = SVGA_HEIGHT,
    .win_regs = OV5645_res_1280x960,
    .frame_rate_array = frame_rate_svga,
};

/* 1600X1200 */
static const struct ov5645_win_size OV5645_win_1600x1200 = {
    .name     = "UXGA",
    .width    = UXGA_WIDTH,
    .height   = UXGA_HEIGHT,
    .win_regs = OV5645_res_1600x1200,
    .frame_rate_array = frame_rate_uxga,
};

/* 640x480 */
static const struct ov5645_win_size OV5645_win_640x480 = {
    .name     = "VGA",
    .width    = 640,
    .height   = 480,
    .win_regs = OV5645_res_640x480,
    .frame_rate_array = frame_rate_uxga,
};
/* 1280x720 */
static const struct ov5645_win_size OV5645_win_1280x720 = {
    .name     = "720p",
    .width    = 1280,
    .height   = 720,
    .win_regs = OV5645_res_1280x720,
    .frame_rate_array = frame_rate_uxga,
};

static const struct ov5645_win_size *ov5645_win[] = {
    &OV5645_win_2592x1944,
    &OV5645_win_1280x960,
    &OV5645_win_1600x1200,
	//&OV5645_win_640x480,
    //&OV5645_win_1280x720
};

/****************************************************************************************
 * general functions
 */
static inline struct ov5645_priv *to_priv(struct v4l2_subdev *subdev)
{
    return container_of(subdev, struct ov5645_priv, subdev);
}

static inline struct v4l2_subdev *ctrl_to_sd(struct v4l2_ctrl *ctrl)
{
    return &container_of(ctrl->handler, struct ov5645_priv, hdl)->subdev;
}

static int reg_read_16(struct i2c_client *client, u16 reg, u8 *val)
{
	int ret;
	/* We have 16-bit i2c addresses - care for endianess */
	unsigned char data[2] = { reg >> 8, reg & 0xff };

	ret = i2c_master_send(client, data, 2);
	if (ret < 2) {
		dev_err(&client->dev, "%s: i2c read error, reg: %x\n",
			__func__, reg);
		return ret < 0 ? ret : -EIO;
	}

	ret = i2c_master_recv(client, val, 1);
	if (ret < 1) {
		dev_err(&client->dev, "%s: i2c read error, reg: %x\n",
				__func__, reg);
		return ret < 0 ? ret : -EIO;
	}
	return 0;
}

static int reg_write_16(struct i2c_client *client, u16 reg, u8 val)
{
	int ret;
	unsigned char data[3] = { reg >> 8, reg & 0xff, val };

	ret = i2c_master_send(client, data, 3);
	if (ret < 0) {
		//dev_err(&client->dev, "%s: i2c write error, reg: %x value: 0x%x\n",	__func__, reg, val);
		return ret < 0 ? ret : -EIO;
		/*
		//return 0;
		while(i++ < 3){	
			printk("try %d, %s reg: %x value: 0x%x\n", i, __func__, reg, val);
			ret = i2c_master_send(client, data, 3);
			if(ret = 3){
				i = 0;
				return 0;
			}
			else if(ret < 3)
				printk("try %d, %s: i2c write error, reg: %x value: 0x%x\n", i, __func__, reg, val);
		}
		return ret < 0 ? ret : -EIO;
		
		i = 0;
		*/
	}
	
	return 0;
}
static int reg_write_array(struct i2c_client *client,
				struct regval_list  *vals)
{
	int i=0;
	while(vals->reg != ARRAY_END_ADDR){
		int ret = reg_write_16(client, vals->reg, vals->value);
		//printk("*");
//		if (ret < 0)
//			return ret;
		#if 0 // for test write sensor reg
			//mdelay(50);
			msleep(10);
			uint8_t value = -1;;

			ret = reg_read_16(client, vals->reg, &value);
			if(vals->value != (value & 0xff))
				printk("[addr]%x, [R] reg[0x:%x] [W]:value:[0x%x]  [R]value[:0x%x]\n",client->addr, vals->reg,vals->value,value);
		#endif
		if(vals->delayms > 0)
			msleep(vals->delayms);
		vals++;
	}
	return 0;
}

static const struct ov5645_win_size *ov5645_select_win(u32 width, u32 height)
{
	const struct ov5645_win_size *win;
    int i;
    printk("%s.............%d...........\n",__func__,__LINE__);

    for (i = 0; i < ARRAY_SIZE(ov5645_win); i++) {
        win = ov5645_win[i];
        if (width == win->width && height == win->height)
            return win;
    }

    printk(KERN_ERR "%s: unsupported width, height (%dx%d)\n", __func__, width, height);
    return NULL;
}

static int ov5645_set_mbusformat(struct i2c_client *client, const struct ov5645_color_format *cfmt)
{

    printk("%s.............%d...........\n",__func__,__LINE__);
    enum v4l2_mbus_pixelcode code;
    int ret = -1;
   /* code = cfmt->code;
    switch (code) {
        case V4L2_MBUS_FMT_YUYV8_2X8:
            ret  = reg_write_array(client, ov5645_fmt_yuv422_yuyv);
            break;
        case V4L2_MBUS_FMT_UYVY8_2X8:
            ret  = reg_write_array(client, ov5645_fmt_yuv422_uyvy);
            break;
        case V4L2_MBUS_FMT_YVYU8_2X8:
            ret  = reg_write_array(client, ov5645_fmt_yuv422_yvyu);
            break;
        case V4L2_MBUS_FMT_VYUY8_2X8:
            ret  = reg_write_array(client, ov5645_fmt_yuv422_vyuy);
            break;
        default:
            printk(KERN_ERR "mbus code error in %s() line %d\n",__FUNCTION__, __LINE__);
    }
    return ret; */
	return 0;
}

static int ov5645_set_params(struct v4l2_subdev *sd, u32 *width, u32 *height, enum v4l2_mbus_pixelcode code)
{
    struct ov5645_priv *priv = to_priv(sd);
    const struct ov5645_win_size *old_win, *new_win;
    int i;
    printk("%s.............%d...........\n",__func__,__LINE__);

    /*
     * select format
     */
    priv->cfmt = NULL;
    for (i = 0; i < ARRAY_SIZE(ov5645_cfmts); i++) {
        if (code == ov5645_cfmts[i].code) {
            priv->cfmt = ov5645_cfmts + i;
            break;
        }
    }
    if (!priv->cfmt) {
        printk(KERN_ERR "Unsupported sensor format.\n");
        return -EINVAL;
    }

    /*
     * select win
     */
    old_win = priv->win;
    new_win = ov5645_select_win(*width, *height);
    if (!new_win) {
        printk(KERN_ERR "Unsupported win size\n");
        return -EINVAL;
    }
    priv->win = new_win;

    priv->rect.left = 0;
    priv->rect.top = 0;
    priv->rect.width = priv->win->width;
    priv->rect.height = priv->win->height;

    *width = priv->win->width;
    *height = priv->win->height;

    return 0;
}

/****************************************************************************************
 * control functions
 */
static int ov5645_set_brightness(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    int value = ctrl->val;
    printk("%s: val %d\n", __func__, ctrl->val);
    unsigned int reg_0xec;
    int ret;

    switch(value){
		case -3:
			ret=reg_write_array(client, OV5645_reg_exposure_n3);
			break;
		case -2:
			ret=reg_write_array(client, OV5645_reg_exposure_n2);
			break;
		case -1:
			ret=reg_write_array(client, OV5645_reg_exposure_n1);
			break;
		case 0:
			ret=reg_write_array(client, OV5645_reg_exposure_0);
			break;
		case 1:
			ret=reg_write_array(client, OV5645_reg_exposure_1);
			break;
		case 2:
			ret=reg_write_array(client, OV5645_reg_exposure_2);
			break;
		case 3:
			ret=reg_write_array(client, OV5645_reg_exposure_3);
			break;
		default:
			//printk("%s:default \n",__func__);
			ret = -EINVAL	;
	}

    return 0;
}

static int ov5645_set_contrast(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    /* TODO */
    int contrast = ctrl->val;
    printk("%s: val %d\n", __func__, contrast);

    return 0;
}

static int ov5645_set_auto_white_balance(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    /* struct ov5645_priv *priv = to_priv(sd); */
    int auto_white_balance = ctrl->val;
    int ret;
    u8 awb;

    printk("%s: val %d\n", __func__, auto_white_balance);
    if (auto_white_balance < 0 || auto_white_balance > 1) {
        dev_err(&client->dev, "set auto_white_balance over range, auto_white_balance = %d\n", auto_white_balance);
        return -ERANGE;
    }

    reg_read_16(client, 0x3406, &awb);
    switch(auto_white_balance) {
        case 0:
            OV5645_DEBUG(KERN_ERR "===awb disable===\n");
            ret = reg_write_16(client, 0x3406, awb&0xfe);
            break;
        case 1:
            OV5645_DEBUG(KERN_ERR "===awb enable===\n");
            ret = reg_write_16(client, 0x3406, awb|0x01);
            break;
    }

    assert(ret == 0);

    return 0;
}

/* TODO : exposure */
static int ov5645_set_exposure(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    printk("%s: val %d\n", __func__, ctrl->val);
    return 0;
}

/* TODO */
static int ov5645_set_gain(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    /* struct i2c_client *client = v4l2_get_subdevdata(sd); */
    /* struct ov5645_priv *priv = to_priv(sd); */
    printk("%s: val %d\n", __func__, ctrl->val);
    return 0;
}

/* TODO */
static int ov5645_set_hflip(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    /* struct i2c_client *client = v4l2_get_subdevdata(sd); */
    /* struct ov5645_priv *priv = to_priv(sd); */
    printk("%s: val %d\n", __func__, ctrl->val);
    return 0;
}
/****************************************
*º¯Êý:ov5645_set_white_balance_temperature
*¹¦ÄÜ:Ñ¡ÔñÅÄÕÕÊ±µÄÆØ¹â
*  V4L2_WHITE_BALANCE_FLUORESCENT:ÈÕ¹âµÆ
* V4L2_WHITE_BALANCE_SUNNY: Ì«Ñô¹â
*V4L2_WHITE_BALANCE_CLOUDY_DAYLIGHT:¶àÔÆÌì
*V4L2_WHITE_BALANCE_TUNGSTEN:ÎÙË¿µÆ
*
*
******************************************/
static int ov5645_set_white_balance_temperature(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    /* struct ov5645_priv *priv = to_priv(sd); */
    int white_balance_temperature = ctrl->val;
    int ret;

    printk("%s: val %d\n", __func__, ctrl->val);

    switch(white_balance_temperature) {
        case V4L2_WHITE_BALANCE_FLUORESCENT:
            ret = reg_write_array(client, OV5645_reg_wb_fluorscent);
            break;
        case V4L2_WHITE_BALANCE_SUNNY:
            ret = reg_write_array(client, OV5645_reg_wb_sunny);
            break;
        case V4L2_WHITE_BALANCE_CLOUDY_DAYLIGHT:
            ret = reg_write_array(client, OV5645_reg_wb_cloudy);
            break;
        case V4L2_WHITE_BALANCE_TUNGSTEN:
            ret = reg_write_array(client, OV5645_reg_wb_tungsten);
            break;
        default:
            dev_err(&client->dev, "set white_balance_temperature over range, white_balance_temperature = %d\n", white_balance_temperature);
            return -ERANGE;
    }

    assert(ret == 0);

    return 0;
}
/***************************
*º¯Êý:ov5645_set_colorfx
*¹¦ÄÜ:Ñ¡ÔñÅÄÕÕÊ±ÕÕÆ¬ÑÕÉ«
****************************/
static int ov5645_set_colorfx(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
     struct ov5645_priv *priv = to_priv(sd); 
    int colorfx = ctrl->val;
    int ret;

    printk("%s: val %d\n", __func__, ctrl->val);

    switch (colorfx) {
        case V4L2_COLORFX_NONE: /* normal */
            ret = reg_write_array(client, ov5645_colorfx_none_regs);
            break;
        case V4L2_COLORFX_BW: /* black and white */
            ret = reg_write_array(client, ov5645_colorfx_bw_regs);
            break;
        case V4L2_COLORFX_SEPIA: /* antique ,¸´¹Å*/
            ret = reg_write_array(client, ov5645_colorfx_sepia_regs);
            break;
        case V4L2_COLORFX_NEGATIVE: /* negative£¬¸ºÆ¬ */
            ret = reg_write_array(client, ov5645_colorfx_negative_regs);
            break;
        default:
            dev_err(&client->dev, "set colorfx over range, colorfx = %d\n", colorfx);
            return -ERANGE;
    }

    assert(ret == 0);
    return 0;
}
/************************************
*º¯Êý:ov5645_set_exposure_auto
*¹¦ÄÜ:ÉèÖÃ×Ô¶¯ÆØ¹â
*************************************/
static int ov5645_set_exposure_auto(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    /* struct ov5645_priv *priv = to_priv(sd); */
    int exposure_auto = ctrl->val;

    /* unsigned int reg_0xec; */
    /* int ret; */

    printk("%s: val %d\n", __func__, ctrl->val);

    if (exposure_auto < 0 || exposure_auto > 1) {
        dev_err(&client->dev, "set exposure_auto over range, exposure_auto = %d\n", exposure_auto);
        return -ERANGE;
    }

    return 0;
}

/* TODO */
static int ov5645_set_scene_exposure(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    printk("%s: val %d\n", __func__, ctrl->val);
    return 0;
}
/********************************
*º¯Êý:ov5645_set_prev_capt_mode
*¹¦ÄÜ: ÉèÖÃ²¶»ñ»òÕßÔ¤ÀÀÆäÖÐÒ»ÖÖÄ£Ê½
**********************************/
static int ov5645_set_prev_capt_mode(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    printk("%s: val %d\n", __func__, ctrl->val);

    switch(ctrl->val) {
        case PREVIEW_MODE:
            priv->prev_capt_mode = ctrl->val;
            break;
        case CAPTURE_MODE:
            priv->prev_capt_mode = ctrl->val;
            break;
        default:
            dev_err(&client->dev, "set_prev_capt_mode over range, prev_capt_mode = %d\n", ctrl->val);
            return -ERANGE;
    }

    return 0;
}
static int ov5645_set_focus_mode(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
	int err=0;
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	struct ov5645_priv *priv = to_priv(sd);
	printk("enter %s:value=%d\n",__func__,ctrl->val);
	switch(ctrl->val){
		case FOCUS_MODE_AUTO:
		case FOCUS_MODE_TOUCH:
		case FOCUS_MODE_TOUCH_FLASH_AUTO:
		case FOCUS_MODE_TOUCH_FLASH_ON:
		case FOCUS_MODE_TOUCH_FLASH_OFF:
			printk("start to single focus\n");
		//	err=i2c_write_array_16(client, OV5645_focus_single);
			priv->focusmode = FOCUS_MODE_AUTO;
			break;

		case FOCUS_MODE_CONTINOUS:
		case FOCUS_MODE_CONTINOUS_XY:
			printk("####start to constant focus\n");
			err=reg_write_array(client, OV5645_focus_constant);
			priv->focusmode = FOCUS_MODE_CONTINOUS;
			break;
		default:
		//	printk("%s:value=%d error\n",__func__,ctrl->value);
			err=-EINVAL;
			break;
	}
	return err;

}


static int ov5645_set_auto_focus(struct v4l2_subdev *sd, int value)
{
    /* int i; */
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    printk("enter %s:value=%d,x=%d,y=%d\n",__func__,value,priv->fx,priv->fy);
    if (value == 0) 
       reg_write_array(client, OV5645_focus_cancel);
    else{ 
       //reg_write_array(client, OV5645_focus_single);
        reg_write_16(client,0x3023,0x01);
        reg_write_16(client,0x3022,0x06); 	
        msleep(10);	
        reg_write_16(client,0x3024,priv->fx);	
        reg_write_16(client,0x3025,priv->fy);   	
        reg_write_16(client,0x3023,0x01);   	
        reg_write_16(client,0x3022,0x81);  	
        msleep(10);	
        reg_write_16(client,0x3023,0x01);	
        reg_write_16(client,0x3022,0x03);
    }
    return 0;
}


static void ov5645_get_focus_result(struct v4l2_subdev *sd, struct v4l2_ctrl *ctrl)
{
	uint8_t state_3028=0;
	uint8_t state_3029=0;
	uint8_t value;
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	reg_read_16(client, 0x3028, &state_3028);
	reg_read_16(client, 0x3029, &state_3029);
	if (state_3028==0)	{
		ctrl->val = SENSOR_AF_ERROR;
	}
	else if (state_3028==1){
		switch (state_3029){
			case 0x70:
				ctrl->val = SENSOR_AF_IDLE;
				break;
			case 0x00:
				ctrl->val = SENSOR_AF_FOCUSING;
				break;
			case 0x10:
				ctrl->val = SENSOR_AF_FOCUSED;
				break;
			case 0x20:
				ctrl->val = SENSOR_AF_FOCUSED;
				break;
			default:
				ctrl->val = SENSOR_AF_SCENE_DETECTING;
				break;
		}
	}
//    printk("enter %s:result=%d\n",__func__,ctrl->val);
}

static int ov5645_s_ctrl(struct v4l2_ctrl *ctrl)
{
    struct v4l2_subdev *sd = ctrl_to_sd(ctrl);
    struct i2c_client *client = v4l2_get_subdevdata(sd);
	  struct ov5645_priv *priv = to_priv(sd);
    int ret = 0;
    unsigned int dx,dy,value;
    printk("%s.............%d...........\n",__func__,__LINE__);

    switch (ctrl->id) {
        case V4L2_CID_BRIGHTNESS:
            ov5645_set_brightness(sd, ctrl);
            break;

        case V4L2_CID_CONTRAST:
            ov5645_set_contrast(sd, ctrl);
            break;

        case V4L2_CID_AUTO_WHITE_BALANCE:
            ov5645_set_auto_white_balance(sd, ctrl);
            break;

        case V4L2_CID_EXPOSURE:
            ov5645_set_exposure(sd, ctrl);
            break;

        case V4L2_CID_GAIN:
            ov5645_set_gain(sd, ctrl);
            break;

        case V4L2_CID_HFLIP:
            ov5645_set_hflip(sd, ctrl);
            break;

        case V4L2_CID_WHITE_BALANCE_TEMPERATURE:
            ov5645_set_white_balance_temperature(sd, ctrl);
            break;

        case V4L2_CID_COLORFX:
            ov5645_set_colorfx(sd, ctrl);
            break;

        case V4L2_CID_EXPOSURE_AUTO:
            ov5645_set_exposure_auto(sd, ctrl);
            break;

        case V4L2_CID_SCENE_EXPOSURE:
            ov5645_set_scene_exposure(sd, ctrl);
            break;

        case V4L2_CID_PRIVATE_PREV_CAPT:
            ov5645_set_prev_capt_mode(sd, ctrl);
            break;
            
   //add by zhengcheng
        case V4L2_CID_DO_WHITE_BALANCE:
            ov5645_set_white_balance_temperature(sd, ctrl);
            break;

        case V4L2_CID_FOCUS_AUTO:
            ov5645_set_auto_focus(sd, ctrl->val);
            break;

        case V4L2_CID_PRIVATE_FOCUS_MODE:
		    ov5645_set_focus_mode(sd,ctrl);
            break;
        case V4L2_CID_PRIVATE_FOCUS_POSITION_X:
            dx = priv->win->width/80;
	          value = ctrl->val/dx;
	        
		   if(value>79)
			      value=79;
	    	    priv->fx = value;
      //    reg_write_16(client,0x3024,value);
            printk("%s,position x=%d,v=%d\n",__func__,ctrl->val,value);
            break;
        case V4L2_CID_PRIVATE_FOCUS_POSITION_Y:
            dy = priv->win->height/60;
	          value = ctrl->val/dy;
		  if(value>59)
			      value=59;
		        priv->fy = value;
       //   reg_write_16(client,0x3025,value);
            printk("%s,position y=%d,v=%d\n",__func__,ctrl->val,value);
            break;

        default:
            dev_err(&client->dev, "%s: invalid control id %d\n", __func__, ctrl->id);
            return -EINVAL;
    }

    return ret;
}
static int ov5645_g_ctrl(struct v4l2_ctrl *ctrl)
{
    struct v4l2_subdev *sd = ctrl_to_sd(ctrl);
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    int ret = 0;
    printk("%s.............%d...........\n",__func__,__LINE__);

    switch (ctrl->id) {
        case V4L2_CID_PRIVATE_FOCUS_RESULT:
             ov5645_get_focus_result(sd,ctrl);
		break;
	  default:
	  	return -EINVAL;
    }
    return 0;
}

static const struct v4l2_ctrl_ops ov5645_ctrl_ops = {
    .g_volatile_ctrl = ov5645_g_ctrl,
	.s_ctrl = ov5645_s_ctrl,
};



static const struct v4l2_ctrl_config ov5645_custom_ctrls[] = {
    {
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_PRIVATE_FOCUS_RESULT,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "FocusResult",
        .min    = 0,
        .max    = 4,
        .def    = 4,
        .step   = 1,
        .flags = V4L2_CTRL_FLAG_VOLATILE,
    }, {
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_PRIVATE_FOCUS_MODE,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "FocusMode",
        .min    = 0,
        .max    = 11,
        .def    = 0,
        .step   = 1,
    },{
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_PRIVATE_FOCUS_POSITION_X,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "FocusPositionX",
        .min    = 1,
        .max    = 2592,
        .def    = 1,
        .step   = 1,
    },{
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_PRIVATE_FOCUS_POSITION_Y,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "FocusPositionY",
        .min    = 1,
        .max    = 1944,
        .def    = 1,
        .step   = 1,
    },{
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_SCENE_EXPOSURE,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "SceneExposure",
        .min    = 0,
        .max    = 1,
        .def    = 0,
        .step   = 1,
    }, {
        .ops    = &ov5645_ctrl_ops,
        .id     = V4L2_CID_PRIVATE_PREV_CAPT,
        .type   = V4L2_CTRL_TYPE_INTEGER,
        .name   = "PrevCapt",
        .min    = 0,
        .max    = 1,
        .def    = 0,
        .step   = 1,
    }
};
/*********************************************
*´Ëº¯Êý½«±»v4l2ÖÐµÄ¿ØÖÆº¯Êý²Ù×÷£¬
*v4l2µÄ²Ù×÷½Ó¿ÚÓÖ±»android  hal ²Ù×÷
*
**********************************************/
static int ov5645_initialize_ctrls(struct ov5645_priv *priv)
{
    printk("%s.............%d...........\n",__func__,__LINE__);

    v4l2_ctrl_handler_init(&priv->hdl, NUM_CTRLS);

    /* standard ctrls */
	priv->auto_focus = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_FOCUS_AUTO, 0, 1, 1, 0);
    if (!priv->auto_focus) {
        pr_err("%s: failed to create focus ctrl\n", __func__);
        return -1;
    }	
	
    priv->white_balance_temperature = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_WHITE_BALANCE_TEMPERATURE, 0, 3, 1, 1);
    if (!priv->white_balance_temperature) {
        printk(KERN_ERR "%s: failed to create white_balance_temperature ctrl\n", __func__);
        return -ENOENT;
    }
    priv->focus_result= v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[0], NULL);
    if (!priv->focus_result) {
        printk(KERN_ERR "%s: failed to create focus result ctrl\n", __func__);
        return -ENOENT;
    }

    priv->focus_mode= v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[1], NULL);
    if (!priv->focus_mode) {
        printk(KERN_ERR "%s: failed to create focus mode ctrl\n", __func__);
        return -ENOENT;
    }

    priv->focus_x= v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[2], NULL);
    if (!priv->focus_x) {
        printk(KERN_ERR "%s: failed to create focus x ctrl\n", __func__);
        return -ENOENT;
    }

    priv->focus_y= v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[3], NULL);
    if (!priv->focus_y) {
        printk(KERN_ERR "%s: failed to create focus y ctrl\n", __func__);
        return -ENOENT;
    }

    priv->subdev.ctrl_handler = &priv->hdl;
    if (priv->hdl.error) {
        printk(KERN_ERR "%s: ctrl handler error(%d)\n", __func__, priv->hdl.error);
        v4l2_ctrl_handler_free(&priv->hdl);
        return -EINVAL;
    }
	
    priv->brightness = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_BRIGHTNESS, 0, 6, 1, 0);
    if (!priv->brightness) {
        printk(KERN_ERR "%s: failed to create brightness ctrl\n", __func__);
        return -ENOENT;
    }

    priv->contrast = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_CONTRAST, -6, 6, 1, 0);
    if (!priv->contrast) {
        printk(KERN_ERR "%s: failed to create contrast ctrl\n", __func__);
        return -ENOENT;
    }

    priv->auto_white_balance = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_AUTO_WHITE_BALANCE, 0, 1, 1, 1);
    if (!priv->auto_white_balance) {
        printk(KERN_ERR "%s: failed to create auto_white_balance ctrl\n", __func__);
        return -ENOENT;
    }
/*
#if 0
    priv->exposure = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_EXPOSURE, 0, 0xFFFF, 1, 500);
    if (!priv->exposure) {
        printk(KERN_ERR "%s: failed to create exposure ctrl\n", __func__);
        return -ENOENT;
    }
#endif
*/
    priv->gain = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_GAIN, 0, 0xFF, 1, 128);
    if (!priv->gain) {
        printk(KERN_ERR "%s: failed to create gain ctrl\n", __func__);
        return -ENOENT;
    }

    priv->hflip = v4l2_ctrl_new_std(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_HFLIP, 0, 1, 1, 0);
    if (!priv->hflip) {
        printk(KERN_ERR "%s: failed to create hflip ctrl\n", __func__);
        return -ENOENT;
    }

    
    /* standard menus */
    priv->colorfx = v4l2_ctrl_new_std_menu(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_COLORFX, 3, 0, 0);
    if (!priv->colorfx) {
        printk(KERN_ERR "%s: failed to create colorfx ctrl\n", __func__);
        return -ENOENT;
    }

    priv->exposure_auto = v4l2_ctrl_new_std_menu(&priv->hdl, &ov5645_ctrl_ops,
            V4L2_CID_EXPOSURE_AUTO, 1, 0, 1);
    if (!priv->exposure_auto) {
        printk(KERN_ERR "%s: failed to create exposure_auto ctrl\n", __func__);
        return -ENOENT;
    }

    /* custom ctrls */
    priv->scene_exposure = v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[0], NULL);
    if (!priv->scene_exposure) {
        printk(KERN_ERR "%s: failed to create scene_exposure ctrl\n", __func__);
        return -ENOENT;
    }

    priv->prev_capt = v4l2_ctrl_new_custom(&priv->hdl, &ov5645_custom_ctrls[1], NULL);
    if (!priv->prev_capt) {
        printk(KERN_ERR "%s: failed to create prev_capt ctrl\n", __func__);
        return -ENOENT;
    }

    priv->subdev.ctrl_handler = &priv->hdl;
    if (priv->hdl.error) {
        printk(KERN_ERR "%s: ctrl handler error(%d)\n", __func__, priv->hdl.error);
        v4l2_ctrl_handler_free(&priv->hdl);
        return -EINVAL;
    }

    return 0;
}

static int ov5645_save_exposure_param(struct v4l2_subdev *sd)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    int ret = 0;
    return ret;
}

static int ov5645_set_exposure_param(struct v4l2_subdev *sd) __attribute__((unused));
static int ov5645_set_exposure_param(struct v4l2_subdev *sd)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    int ret;
    return 0;
}

/****************************************************************************************
 * v4l2 subdev ops
 */

/**
 * core ops
 */
static int ov5645_g_chip_ident(struct v4l2_subdev *sd, struct v4l2_dbg_chip_ident *id)
{
    printk("%s.............%d...........\n",__func__,__LINE__);

    struct ov5645_priv *priv = to_priv(sd);
    id->ident    = priv->model;
    id->revision = 0;
    return 0;
}

static int ov5645_s_power(struct v4l2_subdev *sd, int on)
{
    /* used when suspending */
    /* printk("%s: on %d\n", __func__, on); */
    if (!on) {
        struct ov5645_priv *priv = to_priv(sd);
        priv->initialized = false;
    }
    printk("%s.............%d...........\n",__func__,__LINE__);

    return 0;
}
static void reset_ctrl(struct v4l2_subdev *sd)
{
	struct v4l2_ctrl *ctrl;

	mutex_lock(&sd->ctrl_handler->lock);
	list_for_each_entry(ctrl, &sd->ctrl_handler->ctrls, node){
	if(ctrl->type != V4L2_CTRL_TYPE_STRING)
		ctrl->val = ctrl->cur.val = ctrl->default_value;
	}
	mutex_unlock(&sd->ctrl_handler->lock);
}

static int ov5645_init(struct v4l2_subdev *sd)
{ 
	struct i2c_client *client = v4l2_get_subdevdata(sd);
	int err = -EINVAL;
	uint8_t value = -1,v1,v2;
	printk("enter %s\n",__func__);
	
	reg_read_16(client, 0x300a, &v1);
	reg_read_16(client, 0x300b, &v2);
	if(v1==0x56&&v2==0x45)
		printk("ov5645 camera\n");
	else{
		printk("%s:error,not find 0v5645\n",__func__);
		return -ENODEV;
	}

	reg_write_array(client, ov5645_reset_regs);
	usleep_range(5000, 5500);  // must
   // ov5645_set_auto_focus(sd,1);
	reg_write_array(client, OV5645_auto_focus_init_s1);

	msleep(10);
	err=i2c_master_send(client, OV5645_auto_focus_init_s2, ARRAY_SIZE(OV5645_auto_focus_init_s2));
	//printk("%s:init err=%d\n",__func__,err);
	reg_read_16(client, 0x8000, &value);
	if(value!=0x02){
		printk("###########%s:init error ############\n",__func__);
		printk("###########%s:init error ############\n",__func__);
		printk("###########%s:init error ############\n",__func__);
	}
    
	reg_write_array(client, OV5645_reg_init_2lane);

	reg_write_array(client, OV5645_auto_focus_init_s3);
    
	reset_ctrl(sd);

	return 0;
}


static const struct v4l2_subdev_core_ops ov5645_subdev_core_ops = {
    .g_chip_ident	  = ov5645_g_chip_ident,
    .s_power        = ov5645_s_power,
    .reset          =      reset_ctrl,
    .s_ctrl         = v4l2_subdev_s_ctrl,
	.init           = ov5645_init,
};

/**
 * video ops
 */
static int ov5645_s_stream(struct v4l2_subdev *sd, int enable)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    int ret = 0;

    printk("%s: enable %d, initialized %d\n", __func__, enable, priv->initialized);

 	//ov5645_video_probe(client);
 	
    if (enable) {
        if (!priv->win || !priv->cfmt) {
            dev_err(&client->dev, "norm or win select error\n");
            return -EPERM;
        }

        if ( !priv->initialized ) {
			ret = ov5645_init(sd);
       	 	if (ret < 0) {
        		printk(KERN_ERR "%s: failed to reg_write_array init regs\n", __func__);
        		return -EIO;
        	}
			
            priv->initialized = true;
            //printk(KERN_ERR "%s reg_write_array init regs\n", __func__);
        }

        reg_write_array(v4l2_get_subdevdata(sd), OV5645_reg_stop_stream);
        ret = reg_write_array(client, priv->win->win_regs);
        if (ret < 0) {
            printk(KERN_ERR "%s: failed to reg_write_array win regs\n", __func__);
            return -EIO;
        }
		/* mask this part leading error by rpdzkj ivy 2016.3.11
		printk(KERN_ERR "%s: reg_write_array win regs\n", __func__);   
        ret = ov5645_set_mbusformat(client, priv->cfmt);
        if (ret < 0) {
            printk(KERN_ERR "%s: failed to ov5645_set_mbusformat()\n", __func__);
            return -EIO;
        }
        */
        /* changed to place next line into delay work by rpdzkj ivy 2016.4.21 */
        //reg_write_array(client, OV5645_reg_start_stream);
		//reg_write_array(client, OV5645_reg_start_stream);

       // if(priv->focusmode == FOCUS_MODE_CONTINOUS)             
	    schedule_delayed_work(&priv->delay_work, msecs_to_jiffies(1));
    } else {
    	  cancel_delayed_work_sync(&priv->delay_work); 
          reg_write_array(client, OV5645_reg_stop_stream);
		  //added by rpdzkj ivy 2016.3.30
		  //ov5645_s_powerdown_reset(0);
		  //camera_power_control(0);
		  //priv->initialized = false;
		  //added end
    }
	

    return ret;
}

static int ov5645_enum_framesizes(struct v4l2_subdev *sd, struct v4l2_frmsizeenum *fsize)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    printk("%s.............%d...........\n",__func__,__LINE__);

    if (fsize->index >= ARRAY_SIZE(ov5645_win)) {
        dev_err(&client->dev, "index(%d) is over range %d\n", fsize->index, ARRAY_SIZE(ov5645_win));
        return -EINVAL;
    }

    switch (fsize->pixel_format) {
        case V4L2_PIX_FMT_YUV420:
        case V4L2_PIX_FMT_YUV422P:
        case V4L2_PIX_FMT_NV12:
        case V4L2_PIX_FMT_YUYV:
            fsize->type = V4L2_FRMSIZE_TYPE_DISCRETE;
            fsize->discrete.width = ov5645_win[fsize->index]->width;
            fsize->discrete.height = ov5645_win[fsize->index]->height;
            break;
        default:
            dev_err(&client->dev, "pixel_format(%d) is Unsupported\n", fsize->pixel_format);
            return -EINVAL;
    }

    dev_info(&client->dev, "type %d, width %d, height %d\n", V4L2_FRMSIZE_TYPE_DISCRETE, fsize->discrete.width, fsize->discrete.height);
    return 0;
}

static int ov5645_enum_mbus_fmt(struct v4l2_subdev *sd, unsigned int index,
        enum v4l2_mbus_pixelcode *code)
{
    if (index >= ARRAY_SIZE(ov5645_cfmts))
        return -EINVAL;
    printk("%s.............%d...........\n",__func__,__LINE__);

    *code = ov5645_cfmts[index].code;
    return 0;
}

static int ov5645_g_mbus_fmt(struct v4l2_subdev *sd, struct v4l2_mbus_framefmt *mf)
{
    struct i2c_client *client = v4l2_get_subdevdata(sd);
    struct ov5645_priv *priv = to_priv(sd);
    if (!priv->win || !priv->cfmt) {
        u32 width = SVGA_WIDTH;
        u32 height = SVGA_HEIGHT;
        int ret = ov5645_set_params(sd, &width, &height, V4L2_MBUS_FMT_UYVY8_2X8);
        if (ret < 0) {
            dev_info(&client->dev, "%s, %d\n", __func__, __LINE__);
            return ret;
        }
    }
    printk("%s.............%d...........\n",__func__,__LINE__);

    mf->width   = priv->win->width;
    mf->height  = priv->win->height;
    mf->code    = priv->cfmt->code;
    mf->colorspace  = priv->cfmt->colorspace;
    mf->field   = V4L2_FIELD_NONE;
    dev_info(&client->dev, "%s, %d\n", __func__, __LINE__);
    return 0;
}

static int ov5645_try_mbus_fmt(struct v4l2_subdev *sd,
        struct v4l2_mbus_framefmt *mf)
{
    /* struct i2c_client *client = v4l2_get_subdevdata(sd); */
    struct ov5645_priv *priv = to_priv(sd);
    const struct ov5645_win_size *win;
    int i;
    printk("%s.............%d...........\n",__func__,__LINE__);

    /*
     * select suitable win
     */
    win = ov5645_select_win(mf->width, mf->height);
    if (!win)
        return -EINVAL;

    mf->width   = win->width;
    mf->height  = win->height;
    mf->field   = V4L2_FIELD_NONE;


    for (i = 0; i < ARRAY_SIZE(ov5645_cfmts); i++)
        if (mf->code == ov5645_cfmts[i].code)
            break;

    if (i == ARRAY_SIZE(ov5645_cfmts)) {
        /* Unsupported format requested. Propose either */
        if (priv->cfmt) {
            /* the current one or */
            mf->colorspace = priv->cfmt->colorspace;
            mf->code = priv->cfmt->code;
        } else {
            /* the default one */
            mf->colorspace = ov5645_cfmts[0].colorspace;
            mf->code = ov5645_cfmts[0].code;
        }
    } else {
        /* Also return the colorspace */
        mf->colorspace	= ov5645_cfmts[i].colorspace;
    }

    return 0;
}

static int ov5645_s_mbus_fmt(struct v4l2_subdev *sd, struct v4l2_mbus_framefmt *mf)
{
    /* struct i2c_client *client = v4l2_get_subdevdata(sd); */
    struct ov5645_priv *priv = to_priv(sd);
    printk("%s.............%d...........\n",__func__,__LINE__);

    int ret = ov5645_set_params(sd, &mf->width, &mf->height, mf->code);
    if (!ret)
        mf->colorspace = priv->cfmt->colorspace;

    return ret;
}

static const struct v4l2_subdev_video_ops ov5645_subdev_video_ops = {
    .s_stream               = ov5645_s_stream,
    .enum_framesizes        = ov5645_enum_framesizes,
    .enum_mbus_fmt          = ov5645_enum_mbus_fmt,
    .g_mbus_fmt             = ov5645_g_mbus_fmt,
    .try_mbus_fmt           = ov5645_try_mbus_fmt,
    .s_mbus_fmt             = ov5645_s_mbus_fmt,
};

/**
 * pad ops
 */
static int ov5645_s_fmt(struct v4l2_subdev *sd, struct v4l2_subdev_fh *fh,
        struct v4l2_subdev_format *fmt)
{
    struct v4l2_mbus_framefmt *mf = &fmt->format;
	
    printk("%s: %dx%d\n", __func__, mf->width, mf->height);
    return ov5645_s_mbus_fmt(sd, mf);
	
	
	
}



static const struct v4l2_subdev_pad_ops ov5645_subdev_pad_ops = {
    .set_fmt    = ov5645_s_fmt,
};

/**
 * subdev ops
 */
static const struct v4l2_subdev_ops ov5645_subdev_ops = {
    .core   = &ov5645_subdev_core_ops,
    .video  = &ov5645_subdev_video_ops,
    .pad    = &ov5645_subdev_pad_ops,
};

/**
 * media_entity_operations
 */
static int ov5645_link_setup(struct media_entity *entity,
        const struct media_pad *local,
        const struct media_pad *remote, u32 flags)
{
    printk("%s\n", __func__);
    return 0;
}

static const struct media_entity_operations ov5645_media_ops = {
    .link_setup = ov5645_link_setup,
};

/********************************************************
 *º¯Êý: ov5645_priv_init
 * ¹¦ÄÜ:ÉãÏñÍ·´ò¿ªÊ±ÎªÔ¤ÀÀÄ£Ê½
 *********************************************************/
static void ov5645_priv_init(struct ov5645_priv * priv)
{
    priv->model = V4L2_IDENT_OV5645;
    priv->prev_capt_mode = PREVIEW_MODE;  //³õÊ¼»¯ÎªÔ¤ÀÀÄ£Ê½
    priv->timeperframe.denominator = 12;//30;
    priv->timeperframe.numerator = 1;
    priv->win = &OV5645_win_1280x960; //OV5645_win_640x480 
}

static int ov5645_video_probe(struct i2c_client *client)
{
	int ret;
	u8 id_high, id_low;
	u16 id;

	/* Read sensor Model ID */
	ret = reg_read_16(client, 0x300a, &id_high);
	if (ret < 0)
    {
        printk("OV5645 read high ID error!!!!!!!!!!!!!!!\n");
		return ret;
    }

	id = id_high << 8;

	ret = reg_read_16(client, 0x300b, &id_low);
	if (ret < 0)
    {
        printk("OV5645 read low ID error!!!!!!!!!!!!!!!\n");
		return ret;
    }

	id |= id_low;

	printk("OV5645 Chip ID 0x%04x detected\n", id);

	if (id != 0x5645)
		return -ENODEV;

	return 0;
}



static void ov5645_const_focus_work(struct work_struct *work)
{
	struct ov5645_priv *data =container_of(work, struct ov5645_priv, delay_work.work);
	/* added next line by rpdzkj ivy 2016.4.21 */
	reg_write_array(v4l2_get_subdevdata(&data->subdev), OV5645_reg_start_stream);
	reg_write_array(v4l2_get_subdevdata(&data->subdev), OV5645_focus_constant);
}

static ssize_t ut_reg_show(struct device *dev,
	struct device_attribute *attr, char *buf)
{
	ssize_t len;
	len = sprintf( buf, "usage(hex):\n  echo 0x93 > ut_reg , echo 0x93=0x03 > ut_reg\n");
      return len;
}

static ssize_t ut_reg_store(struct device *dev,
	struct device_attribute *attr, const char *buf, size_t count)
{
	u8 value1 = 0;
	unsigned int value2;
	unsigned int reg;

	struct v4l2_subdev *sd = (struct v4l2_subdev *)dev_get_drvdata(dev);
	
	if(sscanf(buf, "%x=%x", &reg, &value2) == 2)
	{
		reg_write_16(v4l2_get_subdevdata(sd),	reg,value2);
		printk("reg=0x%02x,value=0x%04x\n",reg,value2);
	}
	else if(sscanf(buf, "%x", &reg) == 1)
	{
		reg_read_16(v4l2_get_subdevdata(sd),reg,&value1);
		printk("reg=0x%02x,value=0x%04x\n",reg,value1);
	}
	
	return count;
}

static DEVICE_ATTR(ut_reg, 0666, ut_reg_show, ut_reg_store);


/**********************************
*ov5645_probe
*����ƥ��ʱ����
***********************************/
static int ov5645_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct ov5645_priv *priv;
    struct v4l2_subdev *sd;
    int ret;
printk("%s==========================\n",__func__);
    priv = kzalloc(sizeof(struct ov5645_priv), GFP_KERNEL);
    if (!priv)
        return -ENOMEM;


    printk("%s...................................\n",__func__);
    ov5645_priv_init(priv);

    sd = &priv->subdev;
    strcpy(sd->name, MODULE_NAME);

   ov5645_video_probe(client); 
    /* register subdev */
    v4l2_i2c_subdev_init(sd, client, &ov5645_subdev_ops);

    sd->flags |= V4L2_SUBDEV_FL_HAS_DEVNODE;
    priv->pad.flags = MEDIA_PAD_FL_SOURCE;
    sd->entity.type = MEDIA_ENT_T_V4L2_SUBDEV_SENSOR;
    sd->entity.ops  = &ov5645_media_ops;
    if (media_entity_init(&sd->entity, 1, &priv->pad, 0)) {
        dev_err(&client->dev, "%s: failed to media_entity_init()\n", __func__);
        kfree(priv);
        return -ENOENT;
    }

    ret = ov5645_initialize_ctrls(priv);
    if (ret < 0) {
        printk(KERN_ERR "%s: failed to initialize controls\n", __func__);
        kfree(priv);
        return ret;
    }
    INIT_DELAYED_WORK(&priv->delay_work, ov5645_const_focus_work);
    device_create_file(&client->dev, &dev_attr_ut_reg);
    return 0;
}

static int ov5645_remove(struct i2c_client *client)
{
    struct v4l2_subdev *sd = i2c_get_clientdata(client);
    v4l2_device_unregister_subdev(sd);
    v4l2_ctrl_handler_free(sd->ctrl_handler);
    media_entity_cleanup(&sd->entity);
    kfree(to_priv(sd));
    return 0;
}

static const struct i2c_device_id ov5645_id[] = {
    { MODULE_NAME, 0 },
    { }
};

MODULE_DEVICE_TABLE(i2c, ov5645_id);

static struct i2c_driver ov5645_i2c_driver = {
    .driver = {
        .name = MODULE_NAME,
    },
    .probe    = ov5645_probe,
    .remove   = ov5645_remove,
    .id_table = ov5645_id,
};

module_i2c_driver(ov5645_i2c_driver);

MODULE_DESCRIPTION("SoC Camera driver for ov5645");
MODULE_AUTHOR("caichsh(caichsh@artekmicro.com)");
MODULE_LICENSE("GPL v2");
