#! /bin/bash

if [ $1 == "u-boot" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t u-boot
	elif [ $1 == "kernel" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t kernel
	elif [ $1 == "android" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t android
	elif [ $1 == "module" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t module
	elif [ $1 == "modules" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t modules
	elif [ $1 == "dist" ]
	then
	./device/nexell/tools/build.sh -b s5p6818_drone -t dist
	else
	echo "input error, usage:"
	echo "====================================="
	echo "./make.sh u-boot  to build uboot"
	echo "./make.sh kernel  to build kernel"
	echo "./make.sh android to build android"
	echo "./make.sh module  to build module"
	echo "./make.sh modules to build modules"
	echo "====================================="

	
fi


