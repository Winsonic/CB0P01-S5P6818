#
# Copyright 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := com.googlecode.eyesfree.setorientation
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_CERTIFICATE := PRESIGNED
#LOCAL_OVERRIDES_PACKAGES :=
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
#ifneq (,$(filter xxxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xhdpi.apk
#else ifneq (,$(filter hdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_hdpi.apk
#else ifneq (,$(filter mdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_mdpi.apk
#else
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_alldpi.apk
#endif
#LOCAL_REQUIRED_MODULES :=
#LOCAL_PREBUILT_JNI_LIBS :=
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := File_manager
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_CERTIFICATE := PRESIGNED
#LOCAL_OVERRIDES_PACKAGES :=
LOCAL_SRC_FILES := $(LOCAL_MODULE).apk
#ifneq (,$(filter xxxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xhdpi.apk
#else ifneq (,$(filter hdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_hdpi.apk
#else ifneq (,$(filter mdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_mdpi.apk
#else
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_alldpi.apk
#endif
#LOCAL_REQUIRED_MODULES :=
#LOCAL_PREBUILT_JNI_LIBS :=
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
my_archs := arm x86
my_src_arch := $(call get-prebuilt-src-arch, $(my_archs))
ifeq ($(my_src_arch),arm)
my_src_abi := armeabi-v7a
else ifeq ($(my_src_arch),x86)
my_src_abi := x86
else ifeq ($(my_src_arch),arm64)
my_src_abi := arm64-v8a
else ifeq ($(my_src_arch),x86_64)
my_src_abi := x86_64
endif
LOCAL_MODULE := eGalaxAutoCalib
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
#LOCAL_PRIVILEGED_MODULE :=
LOCAL_CERTIFICATE := PRESIGNED
#LOCAL_OVERRIDES_PACKAGES :=
LOCAL_SRC_FILES := eGalaxCalibrator-release-i2c.apk
#ifneq (,$(filter xxxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xxhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xxhdpi.apk
#else ifneq (,$(filter xhdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_xhdpi.apk
#else ifneq (,$(filter hdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_hdpi.apk
#else ifneq (,$(filter mdpi,$(PRODUCT_AAPT_PREF_CONFIG)))
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_mdpi.apk
#else
#  LOCAL_SRC_FILES := $(LOCAL_MODULE)_alldpi.apk
#endif
#LOCAL_REQUIRED_MODULES :=
LOCAL_PREBUILT_JNI_LIBS := \
    @lib/$(my_src_abi)/libegalaxcalibratorjni.so
LOCAL_MODULE_TARGET_ARCH := $(my_src_arch)
include $(BUILD_PREBUILT)

